import numpy as np

def Promedios(A,B,C):
    #-------- si C = Mediciones; Se reporta los valores medidos
    #-------- si C = Incertidumbres; Se reporta las incertidumbres de los valores medidos
    A=np.array(A) #-- Valores medidos
    B=np.array(B) #-- Incertidumbres de los valores medidos
    ValPro1=len(A[:,0])
    print(ValPro1)
    CM411=0
    IncCM411=0
    CIC11=0
    CIC12=0
    IncCIC11=0
    IncCIC12=0
    PotenciaF11=0
    PotenciaF12=0
    PotenciaF1C=0
    IncPotenciaF11=0
    IncPotenciaF12=0
    IncPotenciaF1C=0
    AlfaF11=0
    AlfaF12=0
    AlfaF1C=0
    IncAlfaF11=0
    IncAlfaF12=0
    IncAlfaF1C=0

    for i in range(0,ValPro1):
    #------------------ MEDICIONES ------------------#    
        #----- Para la CM4 -----#
        CM411=CM411+A[i,0]
        #----- Para la cámara 1 -----#
        CIC11=CIC11+A[i,1]
        PotenciaF11=PotenciaF11+A[i,2]
        AlfaF11=AlfaF11+A[i,3]
        #----- Para la cámara 2 -----#
        CIC12=CIC12+A[i,4]
        PotenciaF12=PotenciaF12+A[i,5]
        AlfaF12=AlfaF12+A[i,6]
        #----- Para la cámara 1 y 2 -----#
        PotenciaF1C=PotenciaF1C+A[i,7]
        AlfaF1C=AlfaF1C+A[i,8]
                
    #---------------- INCERTIDUMBRES ----------------#
        #----- Para la CM4 -----#
        IncCM411=IncCM411+B[i,0]**2
        #----- Para la cámara 1 -----#
        IncCIC11=IncCIC11+B[i,1]**2
        IncPotenciaF11=IncPotenciaF11+B[i,2]**2
        IncAlfaF11=IncAlfaF11+B[i,3]**2
        #----- Para la cámara 2 -----#
        IncCIC12=IncCIC12+B[i,4]**2
        IncPotenciaF12=IncPotenciaF12+B[i,5]**2
        IncAlfaF12=IncAlfaF12+B[i,6]**2
        #----- Para la cámara 1 y 2 -----#
        IncPotenciaF1C=IncPotenciaF1C+B[i,7]**2
        IncAlfaF1C=IncAlfaF1C+B[i,8]**2

    CM411=CM411/ValPro1
    IncCM411=np.sqrt(IncCM411)/ValPro1

    CIC11=CIC11/ValPro1
    IncCIC11=np.sqrt(IncCIC11)/ValPro1
    CIC12=CIC12/ValPro1
    IncCIC12=np.sqrt(IncCIC12)/ValPro1
            
    PotenciaF11=int(round(PotenciaF11/ValPro1,0))
    PotenciaF12=int(round(PotenciaF12/ValPro1,0))
    PotenciaF1C=int(round(PotenciaF1C/ValPro1,0))
    IncPotenciaF11=int(round(np.sqrt(IncPotenciaF11)/ValPro1,0))
    IncPotenciaF12=int(round(np.sqrt(IncPotenciaF12)/ValPro1,0))
    IncPotenciaF1C=int(round(np.sqrt(IncPotenciaF1C)/ValPro1,0))

    AlfaF11=int(round(AlfaF11/ValPro1,0))
    AlfaF12=int(round(AlfaF12/ValPro1,0))
    AlfaF1C=int(round(AlfaF1C/ValPro1,0))
    IncAlfaF11=int(round(np.sqrt(IncAlfaF11)/ValPro1,0))
    IncAlfaF12=int(round(np.sqrt(IncAlfaF12)/ValPro1,0))
    IncAlfaF1C=int(round(np.sqrt(IncAlfaF1C)/ValPro1,0))

    if str(C)=="Mediciones":

        return CM411, CIC11, CIC12, PotenciaF11, PotenciaF12, PotenciaF1C, AlfaF11, AlfaF12, AlfaF1C
    
    if str(C)=="Incertidumbres":

        return IncCM411, IncCIC11, IncCIC12, IncPotenciaF11, IncPotenciaF12, IncPotenciaF1C, IncAlfaF11, IncAlfaF12, IncAlfaF1C
