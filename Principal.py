#-----------------------------------------------------------------------------------------------------------------------------------#
#-----------------------------------------------------------------------------------------------------------------------------------#
#-----------------------------------------------------------------------------------------------------------------------------------#
#-------------------------- Algoritmo para la Estimación de la Potencia térmica del Reactor Nuclear RP-10 --------------------------#
#-----------------------------------------------------------------------------------------------------------------------------------#
#-----------------------------------------------------------------------------------------------------------------------------------#
#-----------------------------------------------------------------------------------------------------------------------------------#

import sys
import numpy as np
import math as mt
from scipy.fftpack import fft
from scipy.optimize import curve_fit
from matplotlib import pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvas
from matplotlib.figure import Figure
from pylab import *
import pyqtgraph as pg

from reportlab.pdfbase.pdfmetrics import registerFont
from reportlab.lib.pagesizes import A4
from reportlab.lib import colors
from reportlab.lib.colors import black
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen import canvas
from reportlab.platypus import Paragraph, Table, TableStyle
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.enums import TA_JUSTIFY, TA_CENTER
from reportlab.graphics.renderPM import PMCanvas

from matplotlib.backends.backend_qt5agg import (NavigationToolbar2QT as NavigationToolbar)
from PyQt5.QtSql import *
from PyQt5.QtWidgets import*
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QIcon, QPixmap, QPainter, QFont
from PyQt5.QtCore import Qt, QModelIndex
from PyQt5.QtWidgets import QDialog, QWidget,QAction, QApplication, qApp, QVBoxLayout, QPushButton, QTableWidget, QTableWidgetItem, QMessageBox, QHBoxLayout, QLineEdit, QTextEdit, QLabel, QGridLayout, QGroupBox, QMenuBar, QStatusBar, QStyle, QFileDialog
import Derivada
import Gauss
import Minimos2

#'-------------------------------------------------------------------------------
#'--N              : N125092021
#'--Usuario        : Paolo Tataje
#'--Fecha          : 25/09/2021
#'--Descripcion    : NAPSD1 
#'-------------------------------------------------------------------------------
#'--N              : N225092021
#'--Usuario        : Paolo Tataje
#'--Fecha          : 25/09/2021
#'--Descripcion    : NAPSD2 
#'-------------------------------------------------------------------------------
#'--N              : N325092021
#'--Usuario        : Paolo Tataje
#'--Fecha          : 25/09/2021
#'--Descripcion    : NAPSD12 
#'-------------------------------------------------------------------------------
class Ui_Ruta(QDialog):
    def __init__(self, parent=None):
        super(Ui_Ruta, self).__init__(None)
        
        self.parent=parent

        self.setWindowTitle("Rutas")
        self.setWindowIcon(QIcon("0.ico"))
        self.setWindowFlags(Qt.WindowCloseButtonHint | Qt.MSWindowsFixedSizeDialogHint)
        self.setFixedSize(400, 200)

        self.initUI()

    def initUI(self):


        self.label= QLabel(self)              
        self.label.setText("Ruta de almacenamiento :")
        self.label.setGeometry(25, 10, 500, 51)

        self.lineEdit=QLineEdit(self)
        self.lineEdit.setGeometry(25,50,350,21)

        self.label_2= QLabel(self)              
        self.label_2.setText("Informe de Calibración N° :")
        self.label_2.setGeometry(25, 70, 600, 51)

        self.lineEdit_2=QLineEdit(self)
        self.lineEdit_2.setGeometry(25,110,350,21)

        self.pushButton=QPushButton(self)
        self.pushButton.setText("Aceptar")
        self.pushButton.setGeometry(170,160,60,21)

        self.pushButton.clicked.connect(self.Aceptar)

        try:
            Ruta=open("ruta.txt",'r')
            Rut=Ruta.readline()
            self.lineEdit.setText(Rut)
        except Exception as e:
            print(e)
        try:
            Info=open("nombre.txt",'r')
            Inf=Info.readline()
            self.lineEdit_2.setText(Inf)
        except Exception as e:
            print(e)

    def Aceptar(self):
        try:
            Ruta=str(self.lineEdit.text())

            ruta=open("ruta.txt",'w')
            ruta.write(Ruta)
            ruta.close()

            Info=str(self.lineEdit_2.text())
            
            info=open("nombre.txt",'w')
            info.write(Info)
            info.close()

            self.lineEdit.setEnabled(False)
            self.lineEdit_2.setEnabled(False)

            mesa='Excelente'
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Information)
            msg.setText("Se registraron con éxito los datos")
            msg.setInformativeText(mesa)
            msg.setWindowTitle("PotCal 1.0")
            msg.exec_()
            
        except Exception as e:
            print(e)
            mesa='Error'
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Verificar los datos ingresados")
            msg.setInformativeText(mesa)
            msg.setWindowTitle("PotCal 1.0")
            msg.exec_()
                                    

       


class Ui_Acerca(QDialog):
    def __init__(self, parent=None):
        super(Ui_Acerca, self).__init__(None)
        
        self.parent=parent

        self.setWindowTitle("Acerca de...")
        self.setWindowIcon(QIcon("0.ico"))
        self.setWindowFlags(Qt.WindowCloseButtonHint | Qt.MSWindowsFixedSizeDialogHint)
        self.setFixedSize(500, 300)

        self.initUI()

    def initUI(self):
        
        self.label= QLabel(self)
        font = QtGui.QFont()
        font.setBold(True)        
        font.setWeight(75)
        font.setFamily("Century")
        font.setPointSize(15)
        self.label.setFont(font)        
        self.label.setText("PotCal 1.0")
        self.label.setGeometry(180, 40, 201, 51)

        self.label_2= QLabel(self)
        font = QtGui.QFont()                    
        font.setFamily("Century")
        font.setPointSize(12)
        self.label_2.setFont(font)        
        self.label_2.setText("El presente software ha sido elaborado por el Laboratorio")
        self.label_2.setGeometry(25, 80, 500, 51)

        self.label_3= QLabel(self)
        font = QtGui.QFont()                    
        font.setFamily("Century")
        font.setPointSize(12)
        self.label_3.setFont(font)        
        self.label_3.setText("de Física Experimental de Reactores del Instituto Peruano")
        self.label_3.setGeometry(25, 100, 500, 51)

        self.label_4= QLabel(self)
        font = QtGui.QFont()                    
        font.setFamily("Century")
        font.setPointSize(12)
        self.label_4.setFont(font)        
        self.label_4.setText("de Energía Nuclear, en conjunto con la Universidad Nacional")
        self.label_4.setGeometry(25, 120, 500, 51)

        self.label_4e= QLabel(self)
        font = QtGui.QFont()                    
        font.setFamily("Century")
        font.setPointSize(12)
        self.label_4e.setFont(font)        
        self.label_4e.setText("de Ingeniería.")
        self.label_4e.setGeometry(25, 140, 500, 51)

        self.label_5= QLabel(self)
        font = QtGui.QFont()                    
        font.setFamily("Century")
        font.setPointSize(12)
        self.label_5.setFont(font)        
        self.label_5.setText("Desarrollador:  Fís. Paolo G. Tataje")
        self.label_5.setGeometry(25, 180, 500, 51)

        self.label_6= QLabel(self)
        font = QtGui.QFont()                    
        font.setFamily("Century")
        font.setPointSize(12)
        self.label_6.setFont(font)        
        self.label_6.setText("Correo:  paolo.tataje.b@uni.pe")
        self.label_6.setGeometry(25, 200, 500, 51)

class Ui_Parametros(QDialog):
    def __init__(self, parent=None):
        super(Ui_Parametros, self).__init__(None)

        self.parent=parent

        self.setWindowTitle("Parámetros Nucleares")
        #self.setWindowIcon(QIcon("0.ico"))
        self.setWindowFlags(Qt.WindowCloseButtonHint | Qt.MSWindowsFixedSizeDialogHint)
        self.setFixedSize(640, 260)

        self.initUI()
        
    def initUI(self):

        self.label=QLabel(self)
        self.label.setText("")
        self.label.setGeometry(120,30,500,200)
        pixmap = QPixmap('ecuacion.jpg')
        self.label.setPixmap(pixmap)

        

        self.label_3=QLabel(self)
        self.label_3.setText("ɣ = ")
        self.label_3.setGeometry(20,35,20,20)

        self.lineEdit=QLineEdit(self)
        self.lineEdit.setGeometry(60,35,60,20)
        self.lineEdit.setText("3.2e-11")
        self.lineEdit.setEnabled(False)


        self.label_5=QLabel(self)
        self.label_5.setText("D = ")
        self.label_5.setGeometry(20,85,20,20)

        self.lineEdit_2=QLineEdit(self)
        self.lineEdit_2.setGeometry(60,85,60,20)


        self.label_7=QLabel(self)
        self.label_7.setText("βef = ")
        self.label_7.setGeometry(20,135,20,20)

        self.lineEdit_3=QLineEdit(self)
        self.lineEdit_3.setGeometry(60,135,60,20)



        self.label_9=QLabel(self)
        self.label_9.setText("L1 = ")
        self.label_9.setGeometry(20,185,20,20)

        self.lineEdit_4=QLineEdit(self)
        self.lineEdit_4.setGeometry(60,185,60,20)


        self.pushButton = QPushButton(self)
        self.pushButton.setText("Aceptar")
        self.pushButton.setGeometry(280,220,60,25)

        self.pushButton.clicked.connect(self.Aceptar)

        try:
            Fun = open('Parametros.txt','r')
            FunD=str(Fun.readline()).split(';')
            self.lineEdit.setText(FunD[0])
            self.lineEdit_2.setText(FunD[1])
            self.lineEdit_3.setText(FunD[2])
            self.lineEdit_4.setText(FunD[3])
        except Exception as e:
            print(e)

    def Aceptar(self):

        try:
            gamma=float(self.lineEdit.text())
            D=float(self.lineEdit_2.text())
            Bef=float(self.lineEdit_3.text())
            L1=float(self.lineEdit_4.text())
            
            FunD=open('Parametros.txt','w')
            FunD.write(str(gamma)+";"+str(D)+";"+str(Bef)+";"+str(L1))
            FunD.close()

            self.lineEdit.setEnabled(False)
            self.lineEdit_2.setEnabled(False)
            self.lineEdit_3.setEnabled(False)
            self.lineEdit_4.setEnabled(False)
            

            mesa='Guardada con éxito'
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Information)
            msg.setText("Parámetros Nucleares")
            msg.setInformativeText(mesa)
            msg.setWindowTitle("PotCal 1.0")
            msg.exec_()

        except Exception as e:
            print(e)
            mesa='Ingresar solo números'
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Parámetros Nucleares.")
            msg.setInformativeText(mesa)
            msg.setWindowTitle("PotCal 1.0")
            msg.exec_()
        

class Ui_Principal(QWidget):
    def __init__(self, parent=None):
        super(Ui_Principal, self).__init__(parent)

        # Creamos una tabla en donde organizaremos los datos
        self.table = QTableWidget(0, 14)
        self.table.setHorizontalHeaderLabels(['I1 (A)','I2 (A)','BW(Hz)', 'P1 (W)', 'P2 (W)','P12 (W)','CM4 (A)', ' α1 (1/s)',' α2 (1/s)',' α12 (1/s)','Fq1','Fq2','e/beta**2 1','e/beta**2 2']) # Tabla 
        self.table.setAlternatingRowColors(True)
        self.table.setEditTriggers(QTableWidget.NoEditTriggers)
        self.table.setSelectionBehavior(QTableWidget.SelectRows)
        self.table.setSelectionMode(QTableWidget.SingleSelection)


        self.label_Title = QLabel("                    Universidad Nacional de Ingeniería - Instituto Peruano de Energía Nuclear                          ")
        font = QtGui.QFont()
        font.setPointSize(16)
        self.label_Title.setFont(font)


        self.label_Imag=QLabel()
        pixmap = QPixmap('uni2.png')
        self.label_Imag.setPixmap(pixmap)

        self.label_Imag2=QLabel()
        pixmap2 = QPixmap('ipen2.png')
        self.label_Imag2.setPixmap(pixmap2)

        self.fig = Figure((1.0, 1.0), dpi=70)
        self.canvas = FigureCanvas(self.fig)        
        self.graph = self.fig.add_subplot(111)
        #### MODIF
        win = pg.GraphicsLayoutWidget(show=True)
        # self.cursorlabel = pg.TextItem(anchor=(-1,10))
        self.cursorlabel = pg.LabelItem(justify='right')
        # self.plt_wid = pg.PlotWidget()
        win.addItem(self.cursorlabel)
        self.plt_wid = win.addPlot(row=1, col=0)
        self.plt_wid.showGrid(True, True)
        self.plt_wid.setLabel('bottom', 'Frecuencia (Hz)')
        self.plt_wid.setLabel('left', 'Densidad Espectral de Potencia Normalizada', 'dB')
        self.plotx = []
        self.ploty = []
        self.plt_data = self.plt_wid.plot(self.plotx, self.ploty,
                    symbol='s', symbolSize=1.5, pen=(255,0,0),
                    symbolBrush=(255,0,0), symbolPen='r',
                    name='Datos'
        )

        self.plt_data2 = self.plt_wid.plot(self.plotx, self.ploty,
                    symbol='s', symbolSize=1.5, pen=(0,0,255),
                    symbolBrush=(0,0,255), symbolPen='b',
                    name='Datos'
        )
                
        self.vLine = pg.InfiniteLine(angle=90, movable=False)
        self.hLine = pg.InfiniteLine(angle=0, movable=False)
        
        self.plt_wid.scene().sigMouseMoved.connect(self.mouseMoved)



        grid = QGridLayout() # Declaramo sun gridlayout en donde ingresaremos todos los widget



        self.label_1 = QLabel("Valores de Potencia :") # Estático
        font = QtGui.QFont()
        font.setPointSize(13)
        self.label_1.setFont(font)
        grid.addWidget(self.label_1,5,2)

        self.label_2 = QLabel("File") # Moficable
        self.label_2.setFont(font)
        grid.addWidget(self.label_2,5,3)

        self.label_3 = QLabel("Potencia (W)") # Estático
        self.label_3.setFont(font)
        grid.addWidget(self.label_3,7,5)

        self.label_s = QLabel(" ") # Estático
        self.label_s.setFont(font)
        grid.addWidget(self.label_s,7,7)


        self.label_4 = QLabel("  α (1/s)  ") # Estático
        self.label_4.setFont(font)
        grid.addWidget(self.label_4,7,8)

        self.label_4R = QLabel("  R^2  ") # Estático
        self.label_4R.setFont(font)
        grid.addWidget(self.label_4R,7,11)

        self.label_5 = QLabel("Cámara 1 :") # Estático
        self.label_5.setFont(font)
        grid.addWidget(self.label_5,9,3)

        self.label_Pot1 = QLabel(" xxx ") # Variable
        self.label_Pot1.setFont(font)
        grid.addWidget(self.label_Pot1,9,5)

        self.label_alfa1 = QLabel(" xxx ") # Variable
        self.label_alfa1.setFont(font)
        grid.addWidget(self.label_alfa1,9,8)

        self.label_R1 = QLabel(" xxx ") # Variable
        self.label_R1.setFont(font)
        grid.addWidget(self.label_R1,9,11)

        self.label_6 = QLabel("Cámara 2 :") # Estático
        self.label_6.setFont(font)
        grid.addWidget(self.label_6,11,3)

        self.label_Pot2 = QLabel(" xxx ") # Variable
        self.label_Pot2.setFont(font)
        grid.addWidget(self.label_Pot2,11,5)

        self.label_alfa2 = QLabel(" xxx ") # Variable
        self.label_alfa2.setFont(font)
        grid.addWidget(self.label_alfa2,11,8)

        self.label_R2 = QLabel(" xxx ") # Variable
        self.label_R2.setFont(font)
        grid.addWidget(self.label_R2,11,11)

        self.label_7 = QLabel("Cruzada :") # Estático
        self.label_7.setFont(font)
        grid.addWidget(self.label_7,13,3)

        self.label_Pot12 = QLabel(" xxx ") # Variable
        self.label_Pot12.setFont(font)
        grid.addWidget(self.label_Pot12,13,5)

        self.label_alfa12 = QLabel(" xxx ") # Variable
        self.label_alfa12.setFont(font)
        grid.addWidget(self.label_alfa12,13,8)

        self.label_R12 = QLabel(" xxx ") # Variable
        self.label_R12.setFont(font)
        grid.addWidget(self.label_R12,13,11)

        self.btn1 = QPushButton('>>') # Boton para graficar espectral 1
        self.btn1.setEnabled(False)
        self.btn1.clicked.connect(self.BTN1)
        
        self.btn2 = QPushButton('>>') # Boton para graficar espectral 2
        self.btn2.setEnabled(False)
        self.btn2.clicked.connect(self.BTN2)

        self.btn3 = QPushButton('>>') # Boton para graficar cruzada
        self.btn3.setEnabled(False)
        self.btn3.clicked.connect(self.BTN3)
        
        grid.addWidget(self.btn1,9,15)
        grid.addWidget(self.btn2,11,15)
        grid.addWidget(self.btn3,13,15)

        grid.addWidget(win,0,17,19,127)

        self.label_t = QLabel("") # Estático
        self.label_t.setStyleSheet("color: blue;")
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_t.setFont(font)
        grid.addWidget(self.label_t,1,25)
                
        self.btnCargar = QPushButton('Calcular') # Boton para cargar y mostrar los datos
        self.btnCargar.clicked.connect(self.calcularDatos) # función al hacer click sobre el boton

        
        self.btnInsertar = QPushButton('Insertar') # Boton agregar datos
        self.btnInsertar.clicked.connect(self.insertarDatos) # función al hacer click sobre el boton

        self.btnEliminar = QPushButton('Eliminar') # Boton para eliminar datos
        self.btnEliminar.clicked.connect(self.eliminarDatos)

        

        self.textEdit_comen=QTextEdit()
        self.textEdit_comen.setPlaceholderText("Resultados de la iteración \n \n")
        self.textEdit_comen.setEnabled(False)
        self.textEdit_comen.setStyleSheet("background-color: #959595") # #f5ffb4
#########
            
        hbx_2=QHBoxLayout()        
        hbx_2.addWidget(self.label_Imag)
        hbx_2.addWidget(self.label_Title)
        hbx_2.addWidget(self.label_Imag2)

        grid_2 = QGridLayout()
        grid_2.addWidget(self.btnCargar,0,1)
        grid_2.addWidget(self.btnInsertar,0,2)
        grid_2.addWidget(self.btnEliminar,0,3)        
        grid_2.addWidget(self.table,1,1,1,3)

        grid_2.addWidget(self.textEdit_comen,0,4,2,4)

        self.menubar = QMenuBar(self)
        

        self.Archivo=self.menubar.addMenu("Archivo")
        self.Abrir = self.Archivo.addAction("Abrir Archivo")
        self.Abrir.triggered.connect(self.Open)
        self.Archivo.addSeparator()
        self.Salir = self.Archivo.addAction("Salir")

        self.Configuracion=self.menubar.addMenu("Configuración")
        self.Parametros = self.Configuracion.addAction("Parámetros Nucleares")
        self.Ruta = self.Configuracion.addAction("Almacenamiento")
        self.Parametros.triggered.connect(self.Parametro)
        self.Ruta.triggered.connect(self.Rut)
        
        

        self.Acerca = self.menubar.addMenu("Acerca de")
        self.Software = self.Acerca.addAction("Software")
        self.Software.triggered.connect(self.Softwar)
        
        vbx = QVBoxLayout()
        vbx.addWidget(self.menubar)        
        vbx.addLayout(hbx_2)
        vbx.addLayout(grid)
        vbx.setAlignment(Qt.AlignCenter)
        vbx.addLayout(grid_2)



        self.setWindowTitle("PotCal 1.0") # Titulo de la ventana
        self.resize(562, 520) # Tamaño de la ventana
        self.setLayout(vbx) # Layout de la ventana

    def Open(self):
        try:
            filename=QFileDialog.getOpenFileName(None, 'Abrir Archivo', filter='Noise Files (*.RUI)',
                                                                 options=QFileDialog.Options())
            ComNom=str(filename)
            print(filename)
            Pa_No=ComNom.split("/")
            try:
                Nam=Pa_No[len(Pa_No)-1].split(".")
                NF=str(Nam[0])+".lbf"
                archi=open(NF,'w')
                archi.close()
            except:
                Nam=Pa_No[0].split(".")
                NF=str(len(Pa_No)-1)+".lbf"
                archi=open(NF,'w')
                archi.close()                
                pass
            if filename[0]:
                data=open(filename[0],'r')                
                with data:
                    data.readline()
                    data.readline()
                    NDat=float(data.readline())               
                    vect=data.readline()
                    vect1=vect.split()
                    vect2=[float(vect1[i]) for i in range(0,3)]
                    Pmues=vect2[0]
                    ICM4=vect2[1]                    
                    ICM4inc=vect2[2]
                    data.readline()
                    data.readline()
                    rangoADC1=float(data.readline())
                    rangoADC2=float(data.readline())
                    data.readline()
                    vect3=data.readline()
                    vect4=vect3.split()
                    vect5=[float(vect4[i]) for i in range(0,4)]
                    I01=vect5[0]
                    InceI01=vect5[1]
                    Fondo1=vect5[2]
                    InceFondo1=vect5[3]
                    vect6=data.readline()
                    vect7=vect6.split()
                    vect8=[float(vect7[i]) for i in range(0,2)]
                    Z1=vect8[0]
                    InceZ1=vect8[1]
                    if Fondo1 == 0:
                        InceF1Z1 = 0
                    else:
                        InceF1Z1=Fondo1*Z1*((InceFondo1/Fondo1)**2+(InceZ1/Z1)**2)**0.5

                    I1=I01-Fondo1*Z1
                    InceI1=np.sqrt((InceI01)**2+(InceF1Z1)**2)
                    I11=I1/Z1 - Fondo1                    
                    InceI11=I11*((InceI1/I1)**2+(InceZ1/Z1)**2)**0.5 ##Incetidumbre
                    vect9=data.readline()
                    vect10=vect9.split()
                    vect11=[float(vect10[i]) for i in range(0,2)]
                    k1=vect11[0]
                    Incek1=vect11[1]
                    data.readline()
                    vect12=data.readline()
                    vect13=vect12.split()
                    vect14=[float(vect13[i]) for i in range(0,4)]
                    I02=vect14[0]
                    InceI02=vect14[1]
                    Fondo2=vect14[2]
                    InceFondo2=vect14[3]
                    vect15=data.readline()
                    vect16=vect15.split()
                    vect17=[float(vect16[i]) for i in range(0,2)]
                    Z2=vect17[0]
                    InceZ2=vect17[1]
                    if Fondo2 == 0:
                        InceF2Z2 = 0
                    else:
                        InceF2Z2=Fondo2*Z2*((InceFondo2/Fondo2)**2+(InceZ2/Z2)**2)**0.5

                    I2=I02-Fondo2*Z2
                    InceI2=np.sqrt((InceI02)**2+(InceF2Z2)**2)
                    I22=I2/Z2 - Fondo2 ##Corriente de la CIC 2
                    InceI22=I22*((InceI2/I2)**2+(InceZ2/Z2)**2)**0.5 ##Incetidumbre de I22                    
                    vect18=data.readline()
                    vect19=vect18.split()
                    vect20=[float(vect19[i]) for i in range(0,2)]               
                    k2=vect20[0]
                    Incek2=vect20[1]
                    vect21=data.readlines()
                    Dat=np.array([int(x) for x in vect21]) # noise measurements 
                    Nu=256 #2*Nu is the measurements number
                    BW=1/(2*Pmues) #band width
                    TM=1/(2*BW-1)
                    T0=Nu/BW                    
                    f=np.arange(1/T0,BW+1/T0,1/T0) #Frecuency vector
                    f=np.array(f)
                    resolucionADC=2**12  #ADC's resolution
                    Dat1=np.zeros(51200) 
                    Dat2=np.zeros(51200)
                    for i in range(0,51200):
                        Dat2[i]=Dat[2*i+1]
                        Dat1[i]=Dat[2*i]
                    Cam1=Dat1                    
                    Cam2=Dat2
                    #-------------- Plot --------------#
                    
                    #plt.subplot(2,1,1)
                    #plt.plot(Cam1,'gray')
                    #grid()
                    #plt.subplot(2,1,2)
                    #plt.plot(Cam1,'gray')
                    #grid()
                    #title(str(Nam[0]))
                    #plt.show()

                    #-------------- End --------------#
                    
                    ProCam1=sum(Cam1)/len(Cam1) #average first chamber 
                    ProCam2=sum(Cam2)/len(Cam2) #average second chamber
                    
                    #--N125092021 - INI
                    #----------------------------------------------------------------------------------------------------------------------#
                    #-------------------------------------------------- Para la Cámara 1 --------------------------------------------------#
                    #----------------------------------------------------------------------------------------------------------------------#
                    DatCam1=Cam1-ProCam1
                    V1=(DatCam1*rangoADC1*2)/(resolucionADC)-Fondo1*Z1                    
                    MV1=np.split(V1,100)                   
                    MV1=np.array(MV1)
                    MV1f=fft(MV1)
                    MV1fc=np.conj(MV1f) 
                    NAPSD1=(2*MV1f*MV1fc)/(T0*(k1**2)*(I1**2)*((2*BW)**2))
                    NAPSD1T=np.array([NAPSD1.real[i] for i in range(0,len(NAPSD1))])
                    NAPSD1t=np.zeros(len(NAPSD1T[0,:]))
                    for i in range(0,len(NAPSD1T[0,:])):
                        NAPSD1t[i]=NAPSD1t[i]+(sum(NAPSD1T[:,i])/100)
                    
                    NAPSD11=np.array(NAPSD1t[0:256])
                    #------------------------- Incert Relativa NAPSD1 -------------------------# 
                    InceReN1=(4*(InceI1/I1)**2+4*(Incek1/k1)**2)**0.5                    
                    #------------------------- Desv. Estandar NAPSD1 -------------------------#
                    DesN1=np.zeros(len(NAPSD1T[0,:]))
                    for i in range(0,len(NAPSD1T[0,:])):                    
                        DesN1[i]=DesN1[i]+np.sqrt(sum((NAPSD1T[:,i]-NAPSD1t[i])**2)/(100*99))
                    #------------------------- Inc. Total -------------------------#
                    InceTN1=np.sqrt((InceReN1*NAPSD11)**2+(DesN1[0:256])**2)

                    P01=[1e-04, 50, 1e-04]
                    def func(f,A1,alfa1,Rd1):
                        return (A1/(1+(2*np.pi*f/alfa1)**2))+Rd1
                    
                    if round(BW)==40:
                        try:                            
                            popt, pcov = curve_fit(func,f[10:250],NAPSD11[10:250],sigma = InceTN1[10:250],p0=P01, absolute_sigma=True)#10
                            residuals = NAPSD11[10:250]- func(f[10:250], *popt)
                            ss_res = np.sum(residuals**2)
                            ss_tot = np.sum((NAPSD11[10:250]-np.mean(NAPSD11[10:250]))**2)
                            R = 1 - (ss_res / ss_tot)

                        except:
                            pass
                            
                    else:
                        try:                            
                            popt, pcov = curve_fit(func,f[2:256],NAPSD11[2:256],sigma = InceTN1[2:256],p0=P01, absolute_sigma=True)
                            residuals = NAPSD11[2:256]- func(f[2:256], *popt)
                            ss_res = np.sum(residuals**2)
                            ss_tot = np.sum((NAPSD11[2:256]-np.mean(NAPSD11[2:256]))**2)
                            R = 1 - (ss_res / ss_tot)

                        except:
                            pass
                        
                    Incer1=np.sqrt(np.diag(pcov))
                    IncerA1=np.abs(Incer1[0])
                    IncerAlfa1=np.abs(Incer1[1])
                    IncerRd1=np.abs(Incer1[2])
                    #-------------------------------------------------- Fin de la Cámara 1 --------------------------------------------------#                        
                    #--N125092021 - FIN
                        
                    #--N225092021 - INI
                    #-------------------------------------------------- Para la Cámara 2 --------------------------------------------------#    
                    DatCam2=Cam2-ProCam2                        
                    V2=(DatCam2*rangoADC2*2)/(resolucionADC)-Fondo2*Z2
                    #-------------- Plot --------------#
                    
                    #plt.subplot(2,1,1)
                    #plt.plot(V1,'gray')
                    #grid()
                    #plt.subplot(2,1,2)
                    #plt.plot(V2,'gray')
                    #grid()
                    #title(str(Nam[0]))
                    #plt.show()

                    #-------------- End --------------#
                    MV2=np.split(V2,100)
                    MV2=np.array(MV2)
                    MV2f=fft(MV2)
                    MV2fc=np.conj(MV2f)
                    NAPSD2=(2*MV2f*MV2fc)/(T0*(k2**2)*(I2**2)*((2*BW)**2))
                    NAPSD2T=np.array([NAPSD2.real[i] for i in range(0,len(NAPSD2))])                        
                    NAPSD2t=np.zeros(len(NAPSD2T[0,:]))

                    for i in range(0,len(NAPSD2T[0,:])):
                        NAPSD2t[i]=NAPSD2t[i]+(sum(NAPSD2T[:,i])/100)

                    #-------------- Plot --------------#

                    #plt.subplot(2,1,1)
                    #plt.plot(NAPSD1t,'b')
                    #grid()
                    #plt.subplot(2,1,2)
                    #plt.plot(NAPSD2t,'b')
                    #grid()
                    #title(str(Nam[0]))
                    #plt.show()

                    #-------------- End --------------#
                        
                    NAPSD22=np.array(NAPSD2t[0:256])
                    #------------------------- Incert Relativa NAPSD2 -------------------------# 
                    InceReN2=(4*(InceI2/I2)**2+4*(Incek2/k2)**2)**0.5                    
                    #------------------------- Desv. Estandar NAPSD2 -------------------------#
                    DesN2=np.zeros(len(NAPSD2T[0,:]))
                    for i in range(0,len(NAPSD2T[0,:])):                    
                        DesN2[i]=DesN2[i]+np.sqrt(sum((NAPSD2T[:,i]-NAPSD2t[i])**2)/(100*99))
                    #------------------------- Inc. Total -------------------------#
                    InceTN2=np.sqrt((InceReN2*NAPSD22)**2+(DesN2[0:256])**2)
                    P02=[1e-04, 50, 1e-04]
                    
                    if round(BW)==40:
                        try:
                            
                            popt2, pcov2 = curve_fit(func,f[10:250],NAPSD22[10:250],sigma = InceTN2[10:250],p0 = P02,absolute_sigma=True)
                            residuals = NAPSD22[10:250]- func(f[10:250], *popt2)
                            ss_res = np.sum(residuals**2)
                            ss_tot = np.sum((NAPSD22[10:250]-np.mean(NAPSD22[10:250]))**2)
                            R2 = 1 - (ss_res / ss_tot)

                        except:
                            pass
                         
                    else:
                        try:
                            
                            popt2, pcov2 = curve_fit(func,f[2:256],NAPSD22[2:256],sigma = InceTN2[2:256],p0 = P02,absolute_sigma=True)
                            residuals = NAPSD22[2:256]- func(f[2:256], *popt2)
                            ss_res = np.sum(residuals**2)
                            ss_tot = np.sum((NAPSD22[2:256]-np.mean(NAPSD22[2:256]))**2)
                            R2 = 1 - (ss_res / ss_tot)

                        except:
                            pass
                        
                    Incer2=np.sqrt(np.diag(pcov2))
                    IncerA2=np.abs(Incer2[0])
                    IncerAlfa2=np.abs(Incer2[1])
                    IncerRd2=np.abs(Incer2[2])

                    #-------------------------------------------------- Fin de la Cámara 2 --------------------------------------------------#
                    #--N225092021 - FIN
                    
                        
                    #--N325092021 - INI
                    #-------------------------------------------------- Para la Cámara 1 y 2 --------------------------------------------------#
                    
                    NCPSD=(2*MV2f*MV1fc)/(T0*(k1*k2)*(I1*I2)*((2*BW)**2))
                    NCPSDT=np.array([NCPSD[i].real for i in range(0,len(NCPSD))])
                    NCPSDt=np.zeros(len(NCPSDT[0,:]))

                    for i in range(0,len(NCPSDT[0,:])):
                        NCPSDt[i]=NCPSDt[i]+(sum(NCPSDT[:,i])/100)

                    NCPSD12=np.array(NCPSDt[0:256])
                    #------------------------- Incert Relativa NCPSD -------------------------# 
                    InceReN12=np.sqrt((Incek1/k1)**2+(Incek2/k2)**2+(InceI1/I1)**2+(InceI2/I2)**2)                  
                    #------------------------- Desv. Estandar NCPSD -------------------------#
                    DesN12=np.zeros(len(NCPSDT[0,:]))
                    for i in range(0,len(NCPSDT[0,:])):                    
                        DesN12[i]=DesN12[i]+np.sqrt(sum((NCPSDT[:,i]-NCPSDt[i])**2)/(100*99))
                    #------------------------- Inc. Total -------------------------#
                    InceTN12=np.sqrt((InceReN12*NCPSD12)**2+(DesN12[0:256])**2)
                    
                    P03=[1e-04,50]
                    def func3(f,A1,alfa1):
                        return (A1/(1+(2*np.pi*f/alfa1)**2))
                    if round(BW)==40:
                        try:                                                     
                            popt3, pcov3 = curve_fit(func3,f[10:250],NCPSD12[10:250],sigma = InceTN12[10:250],p0 = P03,absolute_sigma=True)
                            residuals = NCPSD12[10:250]- func3(f[10:250], *popt3)
                            ss_res = np.sum(residuals**2)
                            ss_tot = np.sum((NCPSD12[10:250]-np.mean(NCPSD12[10:250]))**2)
                            R3 = 1 - (ss_res / ss_tot)                            
                        except:                            
                            pass
                                
                    else:                        
                        try:
                            popt3, pcov3 = curve_fit(func3,f[2:256],NCPSD12[2:256],sigma = InceTN12[2:256],p0 = P03,absolute_sigma=True)
                            residuals = NCPSD12[2:256]- func3(f[2:256], *popt3)
                            ss_res = np.sum(residuals**2)
                            ss_tot = np.sum((NCPSD12[2:256]-np.mean(NCPSD12[2:256]))**2)
                            R3 = 1 - (ss_res / ss_tot)
                        except:
                            pass

                    
                    Incer3=np.sqrt(np.diag(pcov3))
                    IncerA3=np.abs(Incer3[0])
                    IncerAlfa3=np.abs(Incer3[1])
                    #-------------------------------------------------- Fin de la Cámara 1 y 2 --------------------------------------------------#
                    #--N325092021 - FIN

                    Fun = open('Parametros.txt','r')
                    FunD=str(Fun.readline()).split(';')
                      
                    Ef=float(FunD[0])  #---------------------------------------------------------------------------- Energía Media liberada por Fisión [J]
                    L1=float(FunD[3])     #------------------------------------------------------------------------- Factor geométrico asociado al reactor
                    Beff=float(FunD[2]) #--------------------------------------------------------------------------- Fracción Efectiva de los neutrones retardados
                    D=float(FunD[1])     #-------------------------------------------------------------------------- Factor de Diven

                    ebeta1=popt[0]/popt[2]
                    Errebeta1=ebeta1*((IncerA1/np.abs(popt[0]))+(IncerRd1/np.abs(popt[2])))
                    ebeta2=popt2[0]/popt2[2]
                    Errebeta2=ebeta2*((IncerA2/np.abs(popt2[0]))+(IncerRd2/np.abs(popt2[2])))
                    Fq1=popt[2]*I11/2
                    IncerFq1=Fq1*((IncerRd1/np.abs(popt[2]))+(InceI11/np.abs(I11)))
                    Fq2=popt2[2]*I22/2                    
                    IncerFq2=Fq2*((IncerRd2/np.abs(popt2[2]))+(InceI22/np.abs(I22)))
                    
                    #-------------------- Estimación de Potencia y sus Incertidumbres --------------------# 
                    Potencia1=int(round(2*Ef*D*(1-Beff)*L1/(popt[0]*Beff**2),0))
                    Potencia2=int(round(2*Ef*D*(1-Beff)*L1/(popt2[0]*Beff**2),0))
                    Potencia3=int(round(2*Ef*D*(1-Beff)*L1/(popt3[0]*Beff**2),0))
                    ErrPot1=int(round(2*Ef*D*(1-Beff)*L1*IncerA1/(popt[0]**2*Beff**2),0))
                    ErrPot2=int(round(2*Ef*D*(1-Beff)*L1*IncerA2/(popt2[0]**2*Beff**2),0))
                    ErrPot3=int(round(2*Ef*D*(1-Beff)*L1*IncerA3/(popt3[0]**2*Beff**2),0))

                    alfa1=int(np.abs(round(popt[1],0)))
                    alfa2=int(np.abs(round(popt2[1],0)))
                    alfa3=int(np.abs(round(popt3[1],0)))
                    Inceralfa1=int(round(IncerAlfa1,0))
                    Inceralfa2=int(round(IncerAlfa2,0))
                    Inceralfa3=int(round(IncerAlfa3,0))

                    self.label_2.setText(str(Nam[0]))

                    self.label_Pot1.setText(str(Potencia1)+" ± "+str(ErrPot1))
                    self.label_Pot2.setText(str(Potencia2)+" ± "+str(ErrPot2))
                    self.label_Pot12.setText(str(Potencia3)+" ± "+str(ErrPot3))

                    self.label_alfa1.setText(str(alfa1)+" ± "+str(Inceralfa1))
                    self.label_alfa2.setText(str(alfa2)+" ± "+str(Inceralfa2))
                    self.label_alfa12.setText(str(alfa3)+" ± "+str(Inceralfa3))

                    self.label_R1.setText(str(round(R,2)))
                    self.label_R2.setText(str(round(R2,2)))
                    self.label_R12.setText(str(round(R3,2)))

                    self.plotx = f[4:250]
                    self.ploty = NAPSD11[4:250]                    
                    self.ploty2 = NAPSD22[4:250]
                    self.ploty3 = NCPSD12[4:250]

                    NAP1=open("NAP1.txt",'w')

                    for i in range(4,250):
                        NAP1.write(str(f[i])+" "+str(NAPSD11[i])+" "+str(func(f[i],*popt))+"\n")

                    NAP1.write(str(f[250])+" "+str(NAPSD11[250])+" "+str(func(f[250],*popt)))
                    NAP1.close()

                    NAP2=open("NAP2.txt",'w')

                    for i in range(4,250):
                        NAP2.write(str(f[i])+" "+str(NAPSD22[i])+" "+str(func(f[i],*popt2))+"\n")

                    NAP2.write(str(f[250])+" "+str(NAPSD22[250])+" "+str(func(f[250],*popt2)))
                    NAP2.close()

                    NCP=open("NCP.txt",'w')

                    for i in range(4,250):
                        NCP.write(str(f[i])+" "+str(NCPSD12[i])+" "+str(func3(f[i],*popt3))+"\n")

                    NCP.write(str(f[250])+" "+str(NCPSD12[250])+" "+str(func3(f[250],*popt3)))
                    NCP.close()
                        

                    
                    
                    #self.plt_data.setLogMode(False, True)
                    self.plt_data.setData(self.plotx, 10*np.log10(self.ploty))
                    #<self.plt_data2.setLogMode(False, True)
                    self.plt_data2.setData(f[4:250], 10*np.log10(func(f[4:250],*popt)))

                    decim=str(np.format_float_scientific(np.float32(popt[0]))).split("e")
                    self.decimf=np.abs(int(decim[1]))+5

                    decimfg=str(IncerA1).split("e")
                    self.decimfg=np.abs(int(decimfg[1]))+3

                    decimRd=str(np.format_float_scientific(np.float32(popt[2]))).split("e")
                    self.decimRdf=np.abs(int(decimRd[1]))+5

                    decimRdg=str(IncerRd1).split("e")
                    self.decimRdg=np.abs(int(decimRdg[1]))+3
                    

                    decim2=str(np.format_float_scientific(np.float32(popt2[0]))).split("e")
                    self.decimf2=np.abs(int(decim2[1]))+5

                    decimfg2=str(IncerA2).split("e")
                    self.decimfg2=np.abs(int(decimfg2[1]))+3

                    decimRd2=str(np.format_float_scientific(np.float32(popt2[2]))).split("e")
                    self.decimRdf2=np.abs(int(decimRd2[1]))+5

                    decimRdg2=str(IncerRd2).split("e")
                    self.decimRdg2=np.abs(int(decimRdg2[1]))+3
                    

                    decim3=str(np.format_float_scientific(np.float32(popt3[0]))).split("e")
                    self.decimf3=np.abs(int(decim3[1]))+5

                    decimfg3=str(IncerA3).split("e")
                    self.decimfg3=np.abs(int(decimfg3[1]))+3

                    self.A1=popt[0]
                    self.A2=popt2[0]
                    self.A3=popt3[0]

                    self.IncerA1=IncerA1
                    self.IncerA2=IncerA2
                    self.IncerA3=IncerA3

                    self.Alfa1=popt[1]
                    self.Alfa2=popt2[1]
                    self.Alfa3=popt3[1]

                    self.IncerAlfa1=IncerAlfa1
                    self.IncerAlfa2=IncerAlfa2
                    self.IncerAlfa3=IncerAlfa3

                    self.Rd1=popt[2]
                    self.Rd2=popt2[2]
                    self.IncerRd1=IncerRd1
                    self.IncerRd2=IncerRd2

                    self.BW=BW                    
                    self.ebeta1=ebeta1
                    self.Errebeta1=Errebeta1
                    self.ebeta2=ebeta2
                    self.Errebeta2=Errebeta2


                    decimI1=str(np.format_float_scientific(np.float32(I11))).split("e")
                    self.decimfI1=np.abs(int(decimI1[1]))+5
                    
                    self.I1=round(I11,self.decimfI1)
                    self.IncerI1=round(InceI11,self.decimfI1)

                    decimI2=str(np.format_float_scientific(np.float32(I22))).split("e")
                    self.decimfI2=np.abs(int(decimI2[1]))+5

                    self.I2=round(I22,self.decimfI2)
                    self.IncerI2=round(InceI22,self.decimfI2)

                    decimCM4=str(np.format_float_scientific(np.float32(ICM4))).split("e")
                    self.decimCM4f=np.abs(int(decimCM4[1]))+5
                    self.ICM4=round(ICM4,self.decimCM4f)
                    self.ICM4inc=round(ICM4inc,self.decimCM4f)

                    decimFq1=str(np.format_float_scientific(np.float32(Fq1))).split("e")
                    self.decimFq1f=np.abs(int(decimFq1[1]))+5
                    self.Fq1=round(Fq1,self.decimFq1f)
                    self.IncerFq1=round(IncerFq1,self.decimFq1f)

                    decimFq2=str(np.format_float_scientific(np.float32(Fq2))).split("e")
                    self.decimFq2f=np.abs(int(decimFq2[1]))+5
                    self.Fq2=round(Fq2,self.decimFq2f)                 
                    self.IncerFq2=round(IncerFq2,self.decimFq2f)
                    

                    self.label_t.setText("        A[s]: "+str(round(popt[0],self.decimf))+" ± "+str(round(IncerA1,self.decimfg))+"      α[1/s]: "+str(round(popt[1],2))+" ± "+str(round(IncerAlfa1,2))+"      Rd[s]: "+str(round(popt[2],self.decimRdf))+" ± "+str(round(IncerRd1,self.decimRdg)))


                    self.btn1.setEnabled(True)
                    self.btn2.setEnabled(True)
                    self.btn3.setEnabled(True)


        except Exception as e:
            print(e)
            mesa='Error!!'
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Verifique el archivo abierto")
            msg.setInformativeText(mesa)
            msg.setWindowTitle("PotCal 1.0")
            msg.exec_()

    def BTN1(self):
        try:
            NAP1=np.loadtxt("NAP1.txt")
            f=NAP1[:,0]
            NAP=NAP1[:,1]
            NAPAjus=NAP1[:,2]
            
            self.label_t.setText("        A[s]: "+str(round(self.A1,self.decimf))+" ± "+str(round(self.IncerA1,self.decimfg))+"      α[1/s]: "+str(round(self.Alfa1,2))+" ± "+str(round(self.IncerAlfa1,2))+"      Rd[s]: "+str(round(self.Rd1,self.decimRdf))+" ± "+str(round(self.IncerRd1,self.decimRdg)))
            #self.plt_data.setLogMode(False, True)
            self.plt_data.setData(f, 10*np.log10(NAP))
            #self.plt_data2.setLogMode(False, True)
            self.plt_data2.setData(f, 10*np.log10(NAPAjus))
        except Exception as e:
            print(e)
            
    def BTN2(self):
        try:
            NAP2=np.loadtxt("NAP2.txt")
            f=NAP2[:,0]
            NAP=NAP2[:,1]
            NAPAjus=NAP2[:,2]
            
            self.label_t.setText("        A[s]: "+str(round(self.A2,self.decimf2))+" ± "+str(round(self.IncerA2,self.decimfg2))+"      α[1/s]: "+str(round(self.Alfa2,2))+" ± "+str(round(self.IncerAlfa2,2))+"      Rd[s]: "+str(round(self.Rd2,self.decimRdf2))+" ± "+str(round(self.IncerRd2,self.decimRdg2)))
            #self.plt_data.setLogMode(False, True)
            self.plt_data.setData(f, 10*np.log10(NAP))
            #self.plt_data2.setLogMode(False, True)
            self.plt_data2.setData(f, 10*np.log10(NAPAjus))
        except Exception as e:
            print(e)

    def BTN3(self):
        try:
            NAP2=np.loadtxt("NCP.txt")
            f=NAP2[:,0]
            NAP=NAP2[:,1]
            NAPAjus=NAP2[:,2]
            
            self.label_t.setText("        A[s]: "+str(round(self.A3,self.decimf3))+" ± "+str(round(self.IncerA3,self.decimfg3))+"      α[1/s]: "+str(round(self.Alfa3,2))+" ± "+str(round(self.IncerAlfa3,2)))
            #self.plt_data.setLogMode(False, True)
            self.plt_data.setData(f, 10*np.log10(NAP))
            #self.plt_data2.setLogMode(False, True)
            self.plt_data2.setData(f, 10*np.log10(NAPAjus))
        except Exception as e:
            print(e)
                    
            
    def Parametro(self):
        try:
            
            Ui_Parametros(self).exec_() 

        except Exception as e:
            print(e)

    def Softwar(self):
        try:
            
            Ui_Acerca(self).exec_() 

        except Exception as e:
            print(e)

    def Rut(self):
        try:
            
            Ui_Ruta(self).exec_() 

        except Exception as e:
            print(e)

         

        
##########
    def mouseMoved(self, evt):
        pos = evt
        if self.plt_wid.sceneBoundingRect().contains(pos):
            mousePoint = self.plt_wid.vb.mapSceneToView(pos)
            mx = np.array([abs(i-mousePoint.x()) for i in self.plotx])
            index = mx.argmin()
            if index >= 0 and index < len(self.plotx):
                self.cursorlabel.setText(
                    "<span style='font-size: 11pt'>f={:0.2f}, \
                     <span style='color: red'>dB={:0.2f}</span>".format(
                     self.plotx[index], 10*np.log10(self.ploty[index]))
                     )
            self.vLine.setPos(self.plotx[index])
            self.hLine.setPos(10*np.log10(self.ploty[index]))

    def calcularDatos(self):
        try:
            CantRow=int(self.table.rowCount())
            Dat=np.zeros([CantRow,28])
            I1=np.zeros([CantRow,2])
            I2=np.zeros([CantRow,2])
            BW=np.zeros([CantRow,2])
            P1=np.zeros([CantRow,2])
            P2=np.zeros([CantRow,2])
            P12=np.zeros([CantRow,2])
            ICM4=np.zeros([CantRow,2])
            Alfa1=np.zeros([CantRow,2])
            Alfa2=np.zeros([CantRow,2])
            Alfa12=np.zeros([CantRow,2])
            Fq1=np.zeros([CantRow,2])
            Fq2=np.zeros([CantRow,2])
            ebeta1=np.zeros([CantRow,2])
            ebeta2=np.zeros([CantRow,2])
            
            for i in range(0,CantRow):
                for j in range(0,14):
                    DAT=str(self.table.item(i,j).text()).split(' ± ')
                    Dat[i,2*j]=float(DAT[0])
                    try:
                        Dat[i,2*j+1]=float(DAT[1])
                    except:
                        Dat[i,2*j+1]=float(0)

            for i in range(0,CantRow):
                I1[i,0]=Dat[i,0]
                I1[i,1]=Dat[i,1]
                I2[i,0]=Dat[i,2]
                I2[i,1]=Dat[i,3]
                BW[i,0]=Dat[i,4]
                P1[i,0]=Dat[i,6]
                P1[i,1]=Dat[i,7]
                P2[i,0]=Dat[i,8]
                P2[i,1]=Dat[i,9]
                P12[i,0]=Dat[i,10]
                P12[i,1]=Dat[i,11]
                ICM4[i,0]=Dat[i,12]
                ICM4[i,1]=Dat[i,13]
                Alfa1[i,0]=Dat[i,14]
                Alfa1[i,1]=Dat[i,15]
                Alfa2[i,0]=Dat[i,16]
                Alfa2[i,1]=Dat[i,17]
                Alfa12[i,0]=Dat[i,18]
                Alfa12[i,1]=Dat[i,19]
                Fq1[i,0]=Dat[i,20]
                Fq1[i,1]=Dat[i,21]
                Fq2[i,0]=Dat[i,22]
                Fq2[i,1]=Dat[i,23]
                ebeta1[i,0]=Dat[i,24]
                ebeta1[i,1]=Dat[i,25]
                ebeta2[i,0]=Dat[i,26]
                ebeta2[i,1]=Dat[i,27]


            from Intervalo import separacion
            Vec=separacion(ICM4[:,0])

            #--------------------------------------------- Separación matricial por tramos según valor de corriente en la CM4 ---------------------------------------------#
            #------------------------------------------------------------------ PARA LOS VALORES MEDIDOS ------------------------------------------------------------------#
            self.CorrCM4_1=np.zeros([int(Vec[0])+1,9])        
            for i in range(0,len(self.CorrCM4_1[:,0])):
                self.CorrCM4_1[i,:]=[ICM4[i,0], I1[i,0], P1[i,0], Alfa1[i,0], I2[i,0], P2[i,0], Alfa2[i,0], P12[i,0], Alfa12[i,0]]
            
         
            for i in range(1,len(Vec)):
                setattr(self, f"CorrCM4_{i+1}", np.zeros([int(Vec[i]-Vec[i-1]),9]))

            try:
                for i in range(len(self.CorrCM4_1[:,0]),len(self.CorrCM4_1[:,0])+len(self.CorrCM4_2[:,0])):
                    self.CorrCM4_2[i-len(self.CorrCM4_1[:,0]),:]=[ICM4[i,0], I1[i,0], P1[i,0], Alfa1[i,0], I2[i,0], P2[i,0], Alfa2[i,0], P12[i,0], Alfa12[i,0]]
            except:
                pass

            try:
                for i in range(len(self.CorrCM4_1[:,0])+len(self.CorrCM4_2[:,0]),len(self.CorrCM4_1[:,0])+len(self.CorrCM4_2[:,0])+len(self.CorrCM4_3[:,0])):
                    self.CorrCM4_3[i-(len(self.CorrCM4_1[:,0])+len(self.CorrCM4_2[:,0])),:]=[ICM4[i,0], I1[i,0], P1[i,0], Alfa1[i,0], I2[i,0], P2[i,0], Alfa2[i,0], P12[i,0], Alfa12[i,0]]
            except Exception as e:
                print(e)


            try:
                for i in range(len(self.CorrCM4_1[:,0])+len(self.CorrCM4_2[:,0])+len(self.CorrCM4_3[:,0]),len(self.CorrCM4_1[:,0])+len(self.CorrCM4_2[:,0])+len(self.CorrCM4_3[:,0])+len(self.CorrCM4_4[:,0])):
                    self.CorrCM4_4[i-(len(self.CorrCM4_1[:,0])+len(self.CorrCM4_2[:,0])+len(self.CorrCM4_3[:,0])),:]=[ICM4[i,0], I1[i,0], P1[i,0], Alfa1[i,0], I2[i,0], P2[i,0], Alfa2[i,0], P12[i,0], Alfa12[i,0]]
            except Exception as e:
                print(e)

            #------------------------------------------------------------------ PARA LAS INCERT. MEDIDAS ------------------------------------------------------------------#
            self.CorrCM4inc_1=np.zeros([int(Vec[0])+1,9])        
            for i in range(0,len(self.CorrCM4inc_1[:,0])):
                self.CorrCM4inc_1[i,:]=[ICM4[i,1], I1[i,1], P1[i,1], Alfa1[i,1], I2[i,1], P2[i,1], Alfa2[i,1], P12[i,1], Alfa12[i,1]]
            
         
            for i in range(1,len(Vec)):
                setattr(self, f"CorrCM4inc_{i+1}", np.zeros([int(Vec[i]-Vec[i-1]),9]))

            try:
                for i in range(len(self.CorrCM4inc_1[:,0]),len(self.CorrCM4inc_1[:,0])+len(self.CorrCM4inc_2[:,0])):
                    self.CorrCM4inc_2[i-len(self.CorrCM4inc_1[:,0]),:]=[ICM4[i,1], I1[i,1], P1[i,1], Alfa1[i,1], I2[i,1], P2[i,1], Alfa2[i,1], P12[i,1], Alfa12[i,1]]
            except:
                pass

            try:
                for i in range(len(self.CorrCM4inc_1[:,0])+len(self.CorrCM4inc_2[:,0]),len(self.CorrCM4inc_1[:,0])+len(self.CorrCM4inc_2[:,0])+len(self.CorrCM4inc_3[:,0])):
                    self.CorrCM4inc_3[i-(len(self.CorrCM4inc_1[:,0])+len(self.CorrCM4inc_2[:,0])),:]=[ICM4[i,1], I1[i,1], P1[i,1], Alfa1[i,1], I2[i,1], P2[i,1], Alfa2[i,1], P12[i,1], Alfa12[i,1]]
            except Exception as e:
                print(e)


            try:
                for i in range(len(self.CorrCM4inc_1[:,0])+len(self.CorrCM4inc_2[:,0])+len(self.CorrCM4inc_3[:,0]),len(self.CorrCM4inc_1[:,0])+len(self.CorrCM4inc_2[:,0])+len(self.CorrCM4inc_3[:,0])+len(self.CorrCM4inc_4[:,0])):
                    self.CorrCM4inc_4[i-(len(self.CorrCM4inc_1[:,0])+len(self.CorrCM4inc_2[:,0])+len(self.CorrCM4inc_3[:,0])),:]=[ICM4[i,1], I1[i,1], P1[i,1], Alfa1[i,1], I2[i,1], P2[i,1], Alfa2[i,1], P12[i,1], Alfa12[i,1]]
            except Exception as e:
                print(e)                


            import ParametrosNucleares

            #---------------------------------------------  Cálculo de los valores Promediados   ---------------------------------------------#
            
            #-------- Para el primer grupo --------#

            CM411, CIC11, CIC12, PotenciaF11, PotenciaF12, PotenciaF1C, AlfaF11, AlfaF12, AlfaF1C = ParametrosNucleares.Promedios(self.CorrCM4_1,self.CorrCM4inc_1,"Mediciones")
            IncCM411, IncCIC11, IncCIC12, IncPotenciaF11, IncPotenciaF12, IncPotenciaF1C, IncAlfaF11, IncAlfaF12, IncAlfaF1C = ParametrosNucleares.Promedios(self.CorrCM4_1,self.CorrCM4inc_1,"Incertidumbres")

            #-------- Para el segundo grupo --------#
            try:
                CM421, CIC21, CIC22, PotenciaF21, PotenciaF22, PotenciaF2C, AlfaF21, AlfaF22, AlfaF2C = ParametrosNucleares.Promedios(self.CorrCM4_2,self.CorrCM4inc_2,"Mediciones")
                IncCM421, IncCIC21, IncCIC22, IncPotenciaF21, IncPotenciaF22, IncPotenciaF2C, IncAlfaF21, IncAlfaF22, IncAlfaF2C = ParametrosNucleares.Promedios(self.CorrCM4_2,self.CorrCM4inc_2,"Incertidumbres")
            except:
                pass
            #-------- Para el tercer grupo --------#
            try:
                CM431, CIC31, CIC32, PotenciaF31, PotenciaF32, PotenciaF3C, AlfaF31, AlfaF32, AlfaF3C = ParametrosNucleares.Promedios(self.CorrCM4_3,self.CorrCM4inc_3,"Mediciones")
                IncCM431, IncCIC31, IncCIC32, IncPotenciaF31, IncPotenciaF32, IncPotenciaF3C, IncAlfaF31, IncAlfaF32, IncAlfaF3C = ParametrosNucleares.Promedios(self.CorrCM4_3,self.CorrCM4inc_3,"Incertidumbres")
            except:
                pass

            #-------- Para el cuarto grupo --------#
            try:
                CM441, CIC41, CIC42, PotenciaF41, PotenciaF42, PotenciaF4C, AlfaF41, AlfaF42, AlfaF4C = ParametrosNucleares.Promedios(self.CorrCM4_4,self.CorrCM4inc_4,"Mediciones")
                IncCM441, IncCIC41, IncCIC42, IncPotenciaF41, IncPotenciaF42, IncPotenciaF4C, IncAlfaF41, IncAlfaF42, IncAlfaF4C = ParametrosNucleares.Promedios(self.CorrCM4_4,self.CorrCM4inc_4,"Incertidumbres")

            except:
                pass

            
            #--------------------------------------------------------------  Fin   --------------------------------------------------------------#

            #---------------------------------------------  Cálculo de los valores Ponderados   ---------------------------------------------#
            #-------- Para el primer grupo --------#
            PesoAlfa1=[1/IncAlfaF11**2, 1/IncAlfaF12**2, 1/IncAlfaF1C**2]
            AlfaPon1=round((PesoAlfa1[0]*AlfaF11+PesoAlfa1[1]*AlfaF12+PesoAlfa1[2]*AlfaF1C)/sum(PesoAlfa1),0)
            IncAlfaPon1=round(np.sqrt(1/sum(PesoAlfa1)),0)

            PesoPotencia1=[1/IncPotenciaF11**2, 1/IncPotenciaF12**2, 1/IncPotenciaF1C**2]
            PotenciaPon1=round((PesoPotencia1[0]*PotenciaF11+PesoPotencia1[1]*PotenciaF12+PesoPotencia1[2]*PotenciaF1C)/sum(PesoPotencia1),0)
            IncPotenciaPon1=round(np.sqrt(1/sum(PesoPotencia1)),0)
            

            
            try:
                #-------- Para el segundo grupo --------#
                PesoAlfa2=[1/IncAlfaF21**2, 1/IncAlfaF22**2, 1/IncAlfaF2C**2]
                AlfaPon2=round((PesoAlfa2[0]*AlfaF21+PesoAlfa2[1]*AlfaF22+PesoAlfa2[2]*AlfaF2C)/sum(PesoAlfa2),0)
                IncAlfaPon2=round(np.sqrt(1/sum(PesoAlfa2)),0)

                PesoPotencia2=[1/IncPotenciaF21**2, 1/IncPotenciaF22**2, 1/IncPotenciaF2C**2]
                PotenciaPon2=round((PesoPotencia2[0]*PotenciaF21+PesoPotencia2[1]*PotenciaF22+PesoPotencia2[2]*PotenciaF2C)/sum(PesoPotencia2),0)
                IncPotenciaPon2=round(np.sqrt(1/sum(PesoPotencia2)),0)
            except:
                pass

            try:
                #-------- Para el tercer grupo --------#
                PesoAlfa3=[1/IncAlfaF31**2, 1/IncAlfaF32**2, 1/IncAlfaF3C**2]
                AlfaPon3=round((PesoAlfa3[0]*AlfaF31+PesoAlfa3[1]*AlfaF32+PesoAlfa3[2]*AlfaF3C)/sum(PesoAlfa3),0)
                IncAlfaPon3=round(np.sqrt(1/sum(PesoAlfa3)),0)

                PesoPotencia3=[1/IncPotenciaF31**2, 1/IncPotenciaF32**2, 1/IncPotenciaF3C**2]
                PotenciaPon3=round((PesoPotencia3[0]*PotenciaF31+PesoPotencia3[1]*PotenciaF32+PesoPotencia3[2]*PotenciaF3C)/sum(PesoPotencia3),0)
                IncPotenciaPon3=round(np.sqrt(1/sum(PesoPotencia3)),0)
            except:
                pass
            try:
                #-------- Para el cuarto grupo --------#
                PesoAlfa4=[1/IncAlfaF41**2, 1/IncAlfaF42**2, 1/IncAlfaF4C**2]
                AlfaPon4=round((PesoAlfa4[0]*AlfaF41+PesoAlfa4[1]*AlfaF42+PesoAlfa4[2]*AlfaF4C)/sum(PesoAlfa4),0)
                IncAlfaPon4=round(np.sqrt(1/sum(PesoAlfa4)),0)
                
                PesoPotencia4=[1/IncPotenciaF41**2, 1/IncPotenciaF42**2, 1/IncPotenciaF4C**2]
                PotenciaPon4=round((PesoPotencia4[0]*PotenciaF41+PesoPotencia4[1]*PotenciaF42+PesoPotencia4[2]*PotenciaF4C)/sum(PesoPotencia4),0)
                IncPotenciaPon4=round(np.sqrt(1/sum(PesoPotencia4)),0)
            except:
                pass
            
            


            #---------------------------------------  Reporte de la Calibración de la Potencia Térmica ---------------------------------------#

            registerFont (TTFont ('Arial', 'ARIAL.ttf'))
            registerFont (TTFont ('Book', 'BOOKOSB.ttf'))
            registerFont (TTFont ('Arial-Bold', 'ARIALBd.ttf'))
            canv = PMCanvas (10,10)
            ruta=open("ruta.txt",'r')
            ruta=ruta.readline()
            Name=open("nombre.txt",'r')
            Name=Name.readline()
            fileName=str(ruta)+"Reporte de calibración "+str(Name)+".pdf"
            c = canvas.Canvas(fileName, pagesize=A4)
            p = ParagraphStyle(fileName)
            p.textColor = 'black'
            p.alignment = TA_JUSTIFY

            c.setFont ('Book', 13) #Times-Bold
            title="REPORTE DE CALIBRACIÓN N° " +str(Name)
            c.drawString(180,750,title)
            tamle=int(9)
            c.setFont ('Arial', tamle)

            from datetime import date
            Fecha=date.today()
            c.setFont ('Arial-Bold', tamle)
            c.drawString(50,715, "Fecha de emisión : ")
            c.setFont ('Arial', tamle)
            c.drawString(140,715, str(Fecha))

            c.line(30,700,565,700)

            c.drawString(50,680,"Todos los valores de incertidumbres presentes en este apartado son reportados con un favor de cobertura k = 1.")

            if(CantRow==3):
                datad=[
                         ['Detector', 'BW (Hz)', 'I (A)', 'CM4 (A)', 'fq (C)', 'α (Hz)' , ' ε/β^2','Potencia (W)'],
                         ['CIC 1', int(BW[0,0]),str(I1[0,0])+" ± "+str(I1[0,1]),str(ICM4[0,0])+" ± "+str(ICM4[0,1]),str(Fq1[0,0])+" ± "+str(Fq1[0,1]),str(int(Alfa1[0,0]))+" ± "+str(int(Alfa1[0,1])),str(ebeta1[0,0])+" ± "+str(ebeta1[0,1]),str(int(P1[0,0]))+" ± "+str(int(P1[0,1]))],
                         ['CIC 2', int(BW[0,0]),str(I2[0,0])+" ± "+str(I2[0,1]),str(ICM4[0,0])+" ± "+str(ICM4[0,1]),str(Fq2[0,0])+" ± "+str(Fq2[0,1]),str(int(Alfa2[0,0]))+" ± "+str(int(Alfa2[0,1])),str(ebeta2[0,0])+" ± "+str(ebeta2[0,1]),str(int(P2[0,0]))+" ± "+str(int(P2[0,1]))],
                         ['Cruzada', int(BW[0,0]),'-','-','-',str(int(Alfa12[0,0]))+" ± "+str(int(Alfa12[0,1])),'-',str(int(P12[0,0]))+" ± "+str(int(P12[0,1]))],
                         ['CIC 1', int(BW[1,0]),str(I1[1,0])+" ± "+str(I1[1,1]),str(ICM4[1,0])+" ± "+str(ICM4[1,1]),str(Fq1[1,0])+" ± "+str(Fq1[1,1]),str(int(Alfa1[1,0]))+" ± "+str(int(Alfa1[1,1])),str(ebeta1[1,0])+" ± "+str(ebeta1[1,1]),str(int(P1[1,0]))+" ± "+str(int(P1[1,1]))],
                         ['CIC 2', int(BW[1,0]),str(I2[1,0])+" ± "+str(I2[1,1]),str(ICM4[1,0])+" ± "+str(ICM4[1,1]),str(Fq2[1,0])+" ± "+str(Fq2[1,1]),str(int(Alfa2[1,0]))+" ± "+str(int(Alfa2[1,1])),str(ebeta2[1,0])+" ± "+str(ebeta2[1,1]),str(int(P2[1,0]))+" ± "+str(int(P2[1,1]))],
                         ['Cruzada', int(BW[1,0]),'-','-','-',str(int(Alfa12[1,0]))+" ± "+str(int(Alfa12[1,1])),'-',str(int(P12[1,0]))+" ± "+str(int(P12[1,1]))],
                         ['CIC 1', int(BW[2,0]),str(I1[2,0])+" ± "+str(I1[2,1]),str(ICM4[2,0])+" ± "+str(ICM4[2,1]),str(Fq1[2,0])+" ± "+str(Fq1[2,1]),str(int(Alfa1[2,0]))+" ± "+str(int(Alfa1[2,1])),str(ebeta1[2,0])+" ± "+str(ebeta1[2,1]),str(int(P1[2,0]))+" ± "+str(int(P1[2,1]))],
                         ['CIC 2', int(BW[2,0]),str(I2[2,0])+" ± "+str(I2[2,1]),str(ICM4[2,0])+" ± "+str(ICM4[2,1]),str(Fq2[2,0])+" ± "+str(Fq2[2,1]),str(int(Alfa2[2,0]))+" ± "+str(int(Alfa2[2,1])),str(ebeta2[2,0])+" ± "+str(ebeta2[2,1]),str(int(P2[2,0]))+" ± "+str(int(P2[2,1]))],
                         ['Cruzada', int(BW[2,0]),'-','-','-',str(int(Alfa12[2,0]))+" ± "+str(int(Alfa12[2,1])),'-',str(int(P12[2,0]))+" ± "+str(int(P12[2,1]))],
                         ]
                table=Table(datad,[50,30,90,90,90,50,50,80])

                table.setStyle(TableStyle([    
                             ('BACKGROUND', (0,0), (9,0), colors.lightgrey),
                             ('BOX', (0,0), (-1,-1),0.5, colors.black),
                             ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                             ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                             ('VALIGN',(0,-1),(-1,-1),'MIDDLE'),
                             ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                             ('FONTSIZE',(0,0),(-1,-1),7),
                             ('ALIGN', (0,0), (-1,-1), 'CENTER')
                             ]))

                table.wrapOn(c, 10,90)
                table.drawOn(c, 40,480)

            elif (CantRow==4):

                datad=[
                         ['Detector', 'BW (Hz)', 'I (A)', 'CM4 (A)', 'fq (C)', 'α (Hz)' , ' ε/β^2','Potencia (W)'],
                         ['CIC 1', int(BW[0,0]),str(I1[0,0])+" ± "+str(I1[0,1]),str(ICM4[0,0])+" ± "+str(ICM4[0,1]),str(Fq1[0,0])+" ± "+str(Fq1[0,1]),str(int(Alfa1[0,0]))+" ± "+str(int(Alfa1[0,1])),str(ebeta1[0,0])+" ± "+str(ebeta1[0,1]),str(int(P1[0,0]))+" ± "+str(int(P1[0,1]))],
                         ['CIC 2', int(BW[0,0]),str(I2[0,0])+" ± "+str(I2[0,1]),str(ICM4[0,0])+" ± "+str(ICM4[0,1]),str(Fq2[0,0])+" ± "+str(Fq2[0,1]),str(int(Alfa2[0,0]))+" ± "+str(int(Alfa2[0,1])),str(ebeta2[0,0])+" ± "+str(ebeta2[0,1]),str(int(P2[0,0]))+" ± "+str(int(P2[0,1]))],
                         ['Cruzada', int(BW[0,0]),'-','-','-',str(int(Alfa12[0,0]))+" ± "+str(int(Alfa12[0,1])),'-',str(int(P12[0,0]))+" ± "+str(int(P12[0,1]))],
                         ['CIC 1', int(BW[1,0]),str(I1[1,0])+" ± "+str(I1[1,1]),str(ICM4[1,0])+" ± "+str(ICM4[1,1]),str(Fq1[1,0])+" ± "+str(Fq1[1,1]),str(int(Alfa1[1,0]))+" ± "+str(int(Alfa1[1,1])),str(ebeta1[1,0])+" ± "+str(ebeta1[1,1]),str(int(P1[1,0]))+" ± "+str(int(P1[1,1]))],
                         ['CIC 2', int(BW[1,0]),str(I2[1,0])+" ± "+str(I2[1,1]),str(ICM4[1,0])+" ± "+str(ICM4[1,1]),str(Fq2[1,0])+" ± "+str(Fq2[1,1]),str(int(Alfa2[1,0]))+" ± "+str(int(Alfa2[1,1])),str(ebeta2[1,0])+" ± "+str(ebeta2[1,1]),str(int(P2[1,0]))+" ± "+str(int(P2[1,1]))],
                         ['Cruzada', int(BW[1,0]),'-','-','-',str(int(Alfa12[1,0]))+" ± "+str(int(Alfa12[1,1])),'-',str(int(P12[1,0]))+" ± "+str(int(P12[1,1]))],
                         ['CIC 1', int(BW[2,0]),str(I1[2,0])+" ± "+str(I1[2,1]),str(ICM4[2,0])+" ± "+str(ICM4[2,1]),str(Fq1[2,0])+" ± "+str(Fq1[2,1]),str(int(Alfa1[2,0]))+" ± "+str(int(Alfa1[2,1])),str(ebeta1[2,0])+" ± "+str(ebeta1[2,1]),str(int(P1[2,0]))+" ± "+str(int(P1[2,1]))],
                         ['CIC 2', int(BW[2,0]),str(I2[2,0])+" ± "+str(I2[2,1]),str(ICM4[2,0])+" ± "+str(ICM4[2,1]),str(Fq2[2,0])+" ± "+str(Fq2[2,1]),str(int(Alfa2[2,0]))+" ± "+str(int(Alfa2[2,1])),str(ebeta2[2,0])+" ± "+str(ebeta2[2,1]),str(int(P2[2,0]))+" ± "+str(int(P2[2,1]))],
                         ['Cruzada', int(BW[2,0]),'-','-','-',str(int(Alfa12[2,0]))+" ± "+str(int(Alfa12[2,1])),'-',str(int(P12[2,0]))+" ± "+str(int(P12[2,1]))],
                         ['CIC 1', int(BW[3,0]),str(I1[3,0])+" ± "+str(I1[3,1]),str(ICM4[3,0])+" ± "+str(ICM4[3,1]),str(Fq1[3,0])+" ± "+str(Fq1[3,1]),str(int(Alfa1[3,0]))+" ± "+str(int(Alfa1[3,1])),str(ebeta1[3,0])+" ± "+str(ebeta1[3,1]),str(int(P1[3,0]))+" ± "+str(int(P1[3,1]))],
                         ['CIC 2', int(BW[3,0]),str(I2[3,0])+" ± "+str(I2[3,1]),str(ICM4[3,0])+" ± "+str(ICM4[3,1]),str(Fq2[3,0])+" ± "+str(Fq2[3,1]),str(int(Alfa2[3,0]))+" ± "+str(int(Alfa2[3,1])),str(ebeta2[3,0])+" ± "+str(ebeta2[3,1]),str(int(P2[3,0]))+" ± "+str(int(P2[3,1]))],
                         ['Cruzada', int(BW[3,0]),'-','-','-',str(int(Alfa12[3,0]))+" ± "+str(int(Alfa12[3,1])),'-',str(int(P12[3,0]))+" ± "+str(int(P12[3,1]))],
                         ]
                table=Table(datad,[50,30,90,90,90,50,50,80])

                table.setStyle(TableStyle([    
                             ('BACKGROUND', (0,0), (9,0), colors.lightgrey),
                             ('BOX', (0,0), (-1,-1),0.5, colors.black),
                             ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                             ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                             ('VALIGN',(0,-1),(-1,-1),'MIDDLE'),
                             ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                             ('FONTSIZE',(0,0),(-1,-1),7),
                             ('ALIGN', (0,0), (-1,-1), 'CENTER')
                             ]))

                table.wrapOn(c, 10,90)
                table.drawOn(c, 40,426)

            elif (CantRow==5):

                datad=[
                         ['Detector', 'BW (Hz)', 'I (A)', 'CM4 (A)', 'fq (C)', 'α (Hz)' , ' ε/β^2','Potencia (W)'],
                         ['CIC 1', int(BW[0,0]),str(I1[0,0])+" ± "+str(I1[0,1]),str(ICM4[0,0])+" ± "+str(ICM4[0,1]),str(Fq1[0,0])+" ± "+str(Fq1[0,1]),str(int(Alfa1[0,0]))+" ± "+str(int(Alfa1[0,1])),str(ebeta1[0,0])+" ± "+str(ebeta1[0,1]),str(int(P1[0,0]))+" ± "+str(int(P1[0,1]))],
                         ['CIC 2', int(BW[0,0]),str(I2[0,0])+" ± "+str(I2[0,1]),str(ICM4[0,0])+" ± "+str(ICM4[0,1]),str(Fq2[0,0])+" ± "+str(Fq2[0,1]),str(int(Alfa2[0,0]))+" ± "+str(int(Alfa2[0,1])),str(ebeta2[0,0])+" ± "+str(ebeta2[0,1]),str(int(P2[0,0]))+" ± "+str(int(P2[0,1]))],
                         ['Cruzada', int(BW[0,0]),'-','-','-',str(int(Alfa12[0,0]))+" ± "+str(int(Alfa12[0,1])),'-',str(int(P12[0,0]))+" ± "+str(int(P12[0,1]))],
                         ['CIC 1', int(BW[1,0]),str(I1[1,0])+" ± "+str(I1[1,1]),str(ICM4[1,0])+" ± "+str(ICM4[1,1]),str(Fq1[1,0])+" ± "+str(Fq1[1,1]),str(int(Alfa1[1,0]))+" ± "+str(int(Alfa1[1,1])),str(ebeta1[1,0])+" ± "+str(ebeta1[1,1]),str(int(P1[1,0]))+" ± "+str(int(P1[1,1]))],
                         ['CIC 2', int(BW[1,0]),str(I2[1,0])+" ± "+str(I2[1,1]),str(ICM4[1,0])+" ± "+str(ICM4[1,1]),str(Fq2[1,0])+" ± "+str(Fq2[1,1]),str(int(Alfa2[1,0]))+" ± "+str(int(Alfa2[1,1])),str(ebeta2[1,0])+" ± "+str(ebeta2[1,1]),str(int(P2[1,0]))+" ± "+str(int(P2[1,1]))],
                         ['Cruzada', int(BW[1,0]),'-','-','-',str(int(Alfa12[1,0]))+" ± "+str(int(Alfa12[1,1])),'-',str(int(P12[1,0]))+" ± "+str(int(P12[1,1]))],
                         ['CIC 1', int(BW[2,0]),str(I1[2,0])+" ± "+str(I1[2,1]),str(ICM4[2,0])+" ± "+str(ICM4[2,1]),str(Fq1[2,0])+" ± "+str(Fq1[2,1]),str(int(Alfa1[2,0]))+" ± "+str(int(Alfa1[2,1])),str(ebeta1[2,0])+" ± "+str(ebeta1[2,1]),str(int(P1[2,0]))+" ± "+str(int(P1[2,1]))],
                         ['CIC 2', int(BW[2,0]),str(I2[2,0])+" ± "+str(I2[2,1]),str(ICM4[2,0])+" ± "+str(ICM4[2,1]),str(Fq2[2,0])+" ± "+str(Fq2[2,1]),str(int(Alfa2[2,0]))+" ± "+str(int(Alfa2[2,1])),str(ebeta2[2,0])+" ± "+str(ebeta2[2,1]),str(int(P2[2,0]))+" ± "+str(int(P2[2,1]))],
                         ['Cruzada', int(BW[2,0]),'-','-','-',str(int(Alfa12[2,0]))+" ± "+str(int(Alfa12[2,1])),'-',str(int(P12[2,0]))+" ± "+str(int(P12[2,1]))],
                         ['CIC 1', int(BW[3,0]),str(I1[3,0])+" ± "+str(I1[3,1]),str(ICM4[3,0])+" ± "+str(ICM4[3,1]),str(Fq1[3,0])+" ± "+str(Fq1[3,1]),str(int(Alfa1[3,0]))+" ± "+str(int(Alfa1[3,1])),str(ebeta1[3,0])+" ± "+str(ebeta1[3,1]),str(int(P1[3,0]))+" ± "+str(int(P1[3,1]))],
                         ['CIC 2', int(BW[3,0]),str(I2[3,0])+" ± "+str(I2[3,1]),str(ICM4[3,0])+" ± "+str(ICM4[3,1]),str(Fq2[3,0])+" ± "+str(Fq2[3,1]),str(int(Alfa2[3,0]))+" ± "+str(int(Alfa2[3,1])),str(ebeta2[3,0])+" ± "+str(ebeta2[3,1]),str(int(P2[3,0]))+" ± "+str(int(P2[3,1]))],
                         ['Cruzada', int(BW[3,0]),'-','-','-',str(int(Alfa12[3,0]))+" ± "+str(int(Alfa12[3,1])),'-',str(int(P12[3,0]))+" ± "+str(int(P12[3,1]))],
                         ['CIC 1', int(BW[4,0]),str(I1[4,0])+" ± "+str(I1[4,1]),str(ICM4[4,0])+" ± "+str(ICM4[4,1]),str(Fq1[4,0])+" ± "+str(Fq1[4,1]),str(int(Alfa1[4,0]))+" ± "+str(int(Alfa1[4,1])),str(ebeta1[4,0])+" ± "+str(ebeta1[4,1]),str(int(P1[4,0]))+" ± "+str(int(P1[4,1]))],
                         ['CIC 2', int(BW[4,0]),str(I2[4,0])+" ± "+str(I2[4,1]),str(ICM4[4,0])+" ± "+str(ICM4[4,1]),str(Fq2[4,0])+" ± "+str(Fq2[4,1]),str(int(Alfa2[4,0]))+" ± "+str(int(Alfa2[4,1])),str(ebeta2[4,0])+" ± "+str(ebeta2[4,1]),str(int(P2[4,0]))+" ± "+str(int(P2[4,1]))],
                         ['Cruzada', int(BW[4,0]),'-','-','-',str(int(Alfa12[4,0]))+" ± "+str(int(Alfa12[4,1])),'-',str(int(P12[4,0]))+" ± "+str(int(P12[4,1]))],
                         ]
                table=Table(datad,[50,30,90,90,90,50,50,80])

                table.setStyle(TableStyle([    
                             ('BACKGROUND', (0,0), (9,0), colors.lightgrey),
                             ('BOX', (0,0), (-1,-1),0.5, colors.black),
                             ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                             ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                             ('VALIGN',(0,-1),(-1,-1),'MIDDLE'),
                             ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                             ('FONTSIZE',(0,0),(-1,-1),7),
                             ('ALIGN', (0,0), (-1,-1), 'CENTER')
                             ]))

                table.wrapOn(c, 10,90)
                table.drawOn(c, 40,372)

            elif (CantRow==6):

                datad=[
                         ['Detector', 'BW (Hz)', 'I (A)', 'CM4 (A)', 'fq (C)', 'α (Hz)' , ' ε/β^2','Potencia (W)'],
                         ['CIC 1', int(BW[0,0]),str(I1[0,0])+" ± "+str(I1[0,1]),str(ICM4[0,0])+" ± "+str(ICM4[0,1]),str(Fq1[0,0])+" ± "+str(Fq1[0,1]),str(int(Alfa1[0,0]))+" ± "+str(int(Alfa1[0,1])),str(ebeta1[0,0])+" ± "+str(ebeta1[0,1]),str(int(P1[0,0]))+" ± "+str(int(P1[0,1]))],
                         ['CIC 2', int(BW[0,0]),str(I2[0,0])+" ± "+str(I2[0,1]),str(ICM4[0,0])+" ± "+str(ICM4[0,1]),str(Fq2[0,0])+" ± "+str(Fq2[0,1]),str(int(Alfa2[0,0]))+" ± "+str(int(Alfa2[0,1])),str(ebeta2[0,0])+" ± "+str(ebeta2[0,1]),str(int(P2[0,0]))+" ± "+str(int(P2[0,1]))],
                         ['Cruzada', int(BW[0,0]),'-','-','-',str(int(Alfa12[0,0]))+" ± "+str(int(Alfa12[0,1])),'-',str(int(P12[0,0]))+" ± "+str(int(P12[0,1]))],
                         ['CIC 1', int(BW[1,0]),str(I1[1,0])+" ± "+str(I1[1,1]),str(ICM4[1,0])+" ± "+str(ICM4[1,1]),str(Fq1[1,0])+" ± "+str(Fq1[1,1]),str(int(Alfa1[1,0]))+" ± "+str(int(Alfa1[1,1])),str(ebeta1[1,0])+" ± "+str(ebeta1[1,1]),str(int(P1[1,0]))+" ± "+str(int(P1[1,1]))],
                         ['CIC 2', int(BW[1,0]),str(I2[1,0])+" ± "+str(I2[1,1]),str(ICM4[1,0])+" ± "+str(ICM4[1,1]),str(Fq2[1,0])+" ± "+str(Fq2[1,1]),str(int(Alfa2[1,0]))+" ± "+str(int(Alfa2[1,1])),str(ebeta2[1,0])+" ± "+str(ebeta2[1,1]),str(int(P2[1,0]))+" ± "+str(int(P2[1,1]))],
                         ['Cruzada', int(BW[1,0]),'-','-','-',str(int(Alfa12[1,0]))+" ± "+str(int(Alfa12[1,1])),'-',str(int(P12[1,0]))+" ± "+str(int(P12[1,1]))],
                         ['CIC 1', int(BW[2,0]),str(I1[2,0])+" ± "+str(I1[2,1]),str(ICM4[2,0])+" ± "+str(ICM4[2,1]),str(Fq1[2,0])+" ± "+str(Fq1[2,1]),str(int(Alfa1[2,0]))+" ± "+str(int(Alfa1[2,1])),str(ebeta1[2,0])+" ± "+str(ebeta1[2,1]),str(int(P1[2,0]))+" ± "+str(int(P1[2,1]))],
                         ['CIC 2', int(BW[2,0]),str(I2[2,0])+" ± "+str(I2[2,1]),str(ICM4[2,0])+" ± "+str(ICM4[2,1]),str(Fq2[2,0])+" ± "+str(Fq2[2,1]),str(int(Alfa2[2,0]))+" ± "+str(int(Alfa2[2,1])),str(ebeta2[2,0])+" ± "+str(ebeta2[2,1]),str(int(P2[2,0]))+" ± "+str(int(P2[2,1]))],
                         ['Cruzada', int(BW[2,0]),'-','-','-',str(int(Alfa12[2,0]))+" ± "+str(int(Alfa12[2,1])),'-',str(int(P12[2,0]))+" ± "+str(int(P12[2,1]))],
                         ['CIC 1', int(BW[3,0]),str(I1[3,0])+" ± "+str(I1[3,1]),str(ICM4[3,0])+" ± "+str(ICM4[3,1]),str(Fq1[3,0])+" ± "+str(Fq1[3,1]),str(int(Alfa1[3,0]))+" ± "+str(int(Alfa1[3,1])),str(ebeta1[3,0])+" ± "+str(ebeta1[3,1]),str(int(P1[3,0]))+" ± "+str(int(P1[3,1]))],
                         ['CIC 2', int(BW[3,0]),str(I2[3,0])+" ± "+str(I2[3,1]),str(ICM4[3,0])+" ± "+str(ICM4[3,1]),str(Fq2[3,0])+" ± "+str(Fq2[3,1]),str(int(Alfa2[3,0]))+" ± "+str(int(Alfa2[3,1])),str(ebeta2[3,0])+" ± "+str(ebeta2[3,1]),str(int(P2[3,0]))+" ± "+str(int(P2[3,1]))],
                         ['Cruzada', int(BW[3,0]),'-','-','-',str(int(Alfa12[3,0]))+" ± "+str(int(Alfa12[3,1])),'-',str(int(P12[3,0]))+" ± "+str(int(P12[3,1]))],
                         ['CIC 1', int(BW[4,0]),str(I1[4,0])+" ± "+str(I1[4,1]),str(ICM4[4,0])+" ± "+str(ICM4[4,1]),str(Fq1[4,0])+" ± "+str(Fq1[4,1]),str(int(Alfa1[4,0]))+" ± "+str(int(Alfa1[4,1])),str(ebeta1[4,0])+" ± "+str(ebeta1[4,1]),str(int(P1[4,0]))+" ± "+str(int(P1[4,1]))],
                         ['CIC 2', int(BW[4,0]),str(I2[4,0])+" ± "+str(I2[4,1]),str(ICM4[4,0])+" ± "+str(ICM4[4,1]),str(Fq2[4,0])+" ± "+str(Fq2[4,1]),str(int(Alfa2[4,0]))+" ± "+str(int(Alfa2[4,1])),str(ebeta2[4,0])+" ± "+str(ebeta2[4,1]),str(int(P2[4,0]))+" ± "+str(int(P2[4,1]))],
                         ['Cruzada', int(BW[4,0]),'-','-','-',str(int(Alfa12[4,0]))+" ± "+str(int(Alfa12[4,1])),'-',str(int(P12[4,0]))+" ± "+str(int(P12[4,1]))],
                         ['CIC 1', int(BW[5,0]),str(I1[5,0])+" ± "+str(I1[5,1]),str(ICM4[5,0])+" ± "+str(ICM4[5,1]),str(Fq1[5,0])+" ± "+str(Fq1[5,1]),str(int(Alfa1[5,0]))+" ± "+str(int(Alfa1[5,1])),str(ebeta1[5,0])+" ± "+str(ebeta1[5,1]),str(int(P1[5,0]))+" ± "+str(int(P1[5,1]))],
                         ['CIC 2', int(BW[5,0]),str(I2[5,0])+" ± "+str(I2[5,1]),str(ICM4[5,0])+" ± "+str(ICM4[5,1]),str(Fq2[5,0])+" ± "+str(Fq2[5,1]),str(int(Alfa2[5,0]))+" ± "+str(int(Alfa2[5,1])),str(ebeta2[5,0])+" ± "+str(ebeta2[5,1]),str(int(P2[5,0]))+" ± "+str(int(P2[5,1]))],
                         ['Cruzada', int(BW[5,0]),'-','-','-',str(int(Alfa12[5,0]))+" ± "+str(int(Alfa12[5,1])),'-',str(int(P12[5,0]))+" ± "+str(int(P12[5,1]))],
                         ]
                table=Table(datad,[50,30,90,90,90,50,50,80])

                table.setStyle(TableStyle([    
                             ('BACKGROUND', (0,0), (9,0), colors.lightgrey),
                             ('BOX', (0,0), (-1,-1),0.5, colors.black),
                             ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                             ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                             ('VALIGN',(0,-1),(-1,-1),'MIDDLE'),
                             ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                             ('FONTSIZE',(0,0),(-1,-1),7),
                             ('ALIGN', (0,0), (-1,-1), 'CENTER')
                             ]))

                table.wrapOn(c, 10,90)
                table.drawOn(c, 40,318)

            elif (CantRow==7):

                datad=[
                         ['Detector', 'BW (Hz)', 'I (A)', 'CM4 (A)', 'fq (C)', 'α (Hz)' , ' ε/β^2','Potencia (W)'],
                         ['CIC 1', int(BW[0,0]),str(I1[0,0])+" ± "+str(I1[0,1]),str(ICM4[0,0])+" ± "+str(ICM4[0,1]),str(Fq1[0,0])+" ± "+str(Fq1[0,1]),str(int(Alfa1[0,0]))+" ± "+str(int(Alfa1[0,1])),str(ebeta1[0,0])+" ± "+str(ebeta1[0,1]),str(int(P1[0,0]))+" ± "+str(int(P1[0,1]))],
                         ['CIC 2', int(BW[0,0]),str(I2[0,0])+" ± "+str(I2[0,1]),str(ICM4[0,0])+" ± "+str(ICM4[0,1]),str(Fq2[0,0])+" ± "+str(Fq2[0,1]),str(int(Alfa2[0,0]))+" ± "+str(int(Alfa2[0,1])),str(ebeta2[0,0])+" ± "+str(ebeta2[0,1]),str(int(P2[0,0]))+" ± "+str(int(P2[0,1]))],
                         ['Cruzada', int(BW[0,0]),'-','-','-',str(int(Alfa12[0,0]))+" ± "+str(int(Alfa12[0,1])),'-',str(int(P12[0,0]))+" ± "+str(int(P12[0,1]))],
                         ['CIC 1', int(BW[1,0]),str(I1[1,0])+" ± "+str(I1[1,1]),str(ICM4[1,0])+" ± "+str(ICM4[1,1]),str(Fq1[1,0])+" ± "+str(Fq1[1,1]),str(int(Alfa1[1,0]))+" ± "+str(int(Alfa1[1,1])),str(ebeta1[1,0])+" ± "+str(ebeta1[1,1]),str(int(P1[1,0]))+" ± "+str(int(P1[1,1]))],
                         ['CIC 2', int(BW[1,0]),str(I2[1,0])+" ± "+str(I2[1,1]),str(ICM4[1,0])+" ± "+str(ICM4[1,1]),str(Fq2[1,0])+" ± "+str(Fq2[1,1]),str(int(Alfa2[1,0]))+" ± "+str(int(Alfa2[1,1])),str(ebeta2[1,0])+" ± "+str(ebeta2[1,1]),str(int(P2[1,0]))+" ± "+str(int(P2[1,1]))],
                         ['Cruzada', int(BW[1,0]),'-','-','-',str(int(Alfa12[1,0]))+" ± "+str(int(Alfa12[1,1])),'-',str(int(P12[1,0]))+" ± "+str(int(P12[1,1]))],
                         ['CIC 1', int(BW[2,0]),str(I1[2,0])+" ± "+str(I1[2,1]),str(ICM4[2,0])+" ± "+str(ICM4[2,1]),str(Fq1[2,0])+" ± "+str(Fq1[2,1]),str(int(Alfa1[2,0]))+" ± "+str(int(Alfa1[2,1])),str(ebeta1[2,0])+" ± "+str(ebeta1[2,1]),str(int(P1[2,0]))+" ± "+str(int(P1[2,1]))],
                         ['CIC 2', int(BW[2,0]),str(I2[2,0])+" ± "+str(I2[2,1]),str(ICM4[2,0])+" ± "+str(ICM4[2,1]),str(Fq2[2,0])+" ± "+str(Fq2[2,1]),str(int(Alfa2[2,0]))+" ± "+str(int(Alfa2[2,1])),str(ebeta2[2,0])+" ± "+str(ebeta2[2,1]),str(int(P2[2,0]))+" ± "+str(int(P2[2,1]))],
                         ['Cruzada', int(BW[2,0]),'-','-','-',str(int(Alfa12[2,0]))+" ± "+str(int(Alfa12[2,1])),'-',str(int(P12[2,0]))+" ± "+str(int(P12[2,1]))],
                         ['CIC 1', int(BW[3,0]),str(I1[3,0])+" ± "+str(I1[3,1]),str(ICM4[3,0])+" ± "+str(ICM4[3,1]),str(Fq1[3,0])+" ± "+str(Fq1[3,1]),str(int(Alfa1[3,0]))+" ± "+str(int(Alfa1[3,1])),str(ebeta1[3,0])+" ± "+str(ebeta1[3,1]),str(int(P1[3,0]))+" ± "+str(int(P1[3,1]))],
                         ['CIC 2', int(BW[3,0]),str(I2[3,0])+" ± "+str(I2[3,1]),str(ICM4[3,0])+" ± "+str(ICM4[3,1]),str(Fq2[3,0])+" ± "+str(Fq2[3,1]),str(int(Alfa2[3,0]))+" ± "+str(int(Alfa2[3,1])),str(ebeta2[3,0])+" ± "+str(ebeta2[3,1]),str(int(P2[3,0]))+" ± "+str(int(P2[3,1]))],
                         ['Cruzada', int(BW[3,0]),'-','-','-',str(int(Alfa12[3,0]))+" ± "+str(int(Alfa12[3,1])),'-',str(int(P12[3,0]))+" ± "+str(int(P12[3,1]))],
                         ['CIC 1', int(BW[4,0]),str(I1[4,0])+" ± "+str(I1[4,1]),str(ICM4[4,0])+" ± "+str(ICM4[4,1]),str(Fq1[4,0])+" ± "+str(Fq1[4,1]),str(int(Alfa1[4,0]))+" ± "+str(int(Alfa1[4,1])),str(ebeta1[4,0])+" ± "+str(ebeta1[4,1]),str(int(P1[4,0]))+" ± "+str(int(P1[4,1]))],
                         ['CIC 2', int(BW[4,0]),str(I2[4,0])+" ± "+str(I2[4,1]),str(ICM4[4,0])+" ± "+str(ICM4[4,1]),str(Fq2[4,0])+" ± "+str(Fq2[4,1]),str(int(Alfa2[4,0]))+" ± "+str(int(Alfa2[4,1])),str(ebeta2[4,0])+" ± "+str(ebeta2[4,1]),str(int(P2[4,0]))+" ± "+str(int(P2[4,1]))],
                         ['Cruzada', int(BW[4,0]),'-','-','-',str(int(Alfa12[4,0]))+" ± "+str(int(Alfa12[4,1])),'-',str(int(P12[4,0]))+" ± "+str(int(P12[4,1]))],
                         ['CIC 1', int(BW[5,0]),str(I1[5,0])+" ± "+str(I1[5,1]),str(ICM4[5,0])+" ± "+str(ICM4[5,1]),str(Fq1[5,0])+" ± "+str(Fq1[5,1]),str(int(Alfa1[5,0]))+" ± "+str(int(Alfa1[5,1])),str(ebeta1[5,0])+" ± "+str(ebeta1[5,1]),str(int(P1[5,0]))+" ± "+str(int(P1[5,1]))],
                         ['CIC 2', int(BW[5,0]),str(I2[5,0])+" ± "+str(I2[5,1]),str(ICM4[5,0])+" ± "+str(ICM4[5,1]),str(Fq2[5,0])+" ± "+str(Fq2[5,1]),str(int(Alfa2[5,0]))+" ± "+str(int(Alfa2[5,1])),str(ebeta2[5,0])+" ± "+str(ebeta2[5,1]),str(int(P2[5,0]))+" ± "+str(int(P2[5,1]))],
                         ['Cruzada', int(BW[5,0]),'-','-','-',str(int(Alfa12[5,0]))+" ± "+str(int(Alfa12[5,1])),'-',str(int(P12[5,0]))+" ± "+str(int(P12[5,1]))],
                         ['CIC 1', int(BW[6,0]),str(I1[6,0])+" ± "+str(I1[6,1]),str(ICM4[6,0])+" ± "+str(ICM4[6,1]),str(Fq1[6,0])+" ± "+str(Fq1[6,1]),str(int(Alfa1[6,0]))+" ± "+str(int(Alfa1[6,1])),str(ebeta1[6,0])+" ± "+str(ebeta1[6,1]),str(int(P1[6,0]))+" ± "+str(int(P1[6,1]))],
                         ['CIC 2', int(BW[6,0]),str(I2[6,0])+" ± "+str(I2[6,1]),str(ICM4[6,0])+" ± "+str(ICM4[6,1]),str(Fq2[6,0])+" ± "+str(Fq2[6,1]),str(int(Alfa2[6,0]))+" ± "+str(int(Alfa2[6,1])),str(ebeta2[6,0])+" ± "+str(ebeta2[6,1]),str(int(P2[6,0]))+" ± "+str(int(P2[6,1]))],
                         ['Cruzada', int(BW[6,0]),'-','-','-',str(int(Alfa12[6,0]))+" ± "+str(int(Alfa12[6,1])),'-',str(int(P12[6,0]))+" ± "+str(int(P12[6,1]))],
                         ]
                table=Table(datad,[50,30,90,90,90,50,50,80])

                table.setStyle(TableStyle([    
                             ('BACKGROUND', (0,0), (9,0), colors.lightgrey),
                             ('BOX', (0,0), (-1,-1),0.5, colors.black),
                             ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                             ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                             ('VALIGN',(0,-1),(-1,-1),'MIDDLE'),
                             ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                             ('FONTSIZE',(0,0),(-1,-1),7),
                             ('ALIGN', (0,0), (-1,-1), 'CENTER')
                             ]))

                table.wrapOn(c, 10,90)
                table.drawOn(c, 40,264)

            elif (CantRow==8):

                datad=[
                         ['Detector', 'BW (Hz)', 'I (A)', 'CM4 (A)', 'fq (C)', 'α (Hz)' , ' ε/β^2','Potencia (W)'],
                         ['CIC 1', int(BW[0,0]),str(I1[0,0])+" ± "+str(I1[0,1]),str(ICM4[0,0])+" ± "+str(ICM4[0,1]),str(Fq1[0,0])+" ± "+str(Fq1[0,1]),str(int(Alfa1[0,0]))+" ± "+str(int(Alfa1[0,1])),str(ebeta1[0,0])+" ± "+str(ebeta1[0,1]),str(int(P1[0,0]))+" ± "+str(int(P1[0,1]))],
                         ['CIC 2', int(BW[0,0]),str(I2[0,0])+" ± "+str(I2[0,1]),str(ICM4[0,0])+" ± "+str(ICM4[0,1]),str(Fq2[0,0])+" ± "+str(Fq2[0,1]),str(int(Alfa2[0,0]))+" ± "+str(int(Alfa2[0,1])),str(ebeta2[0,0])+" ± "+str(ebeta2[0,1]),str(int(P2[0,0]))+" ± "+str(int(P2[0,1]))],
                         ['Cruzada', int(BW[0,0]),'-','-','-',str(int(Alfa12[0,0]))+" ± "+str(int(Alfa12[0,1])),'-',str(int(P12[0,0]))+" ± "+str(int(P12[0,1]))],
                         ['CIC 1', int(BW[1,0]),str(I1[1,0])+" ± "+str(I1[1,1]),str(ICM4[1,0])+" ± "+str(ICM4[1,1]),str(Fq1[1,0])+" ± "+str(Fq1[1,1]),str(int(Alfa1[1,0]))+" ± "+str(int(Alfa1[1,1])),str(ebeta1[1,0])+" ± "+str(ebeta1[1,1]),str(int(P1[1,0]))+" ± "+str(int(P1[1,1]))],
                         ['CIC 2', int(BW[1,0]),str(I2[1,0])+" ± "+str(I2[1,1]),str(ICM4[1,0])+" ± "+str(ICM4[1,1]),str(Fq2[1,0])+" ± "+str(Fq2[1,1]),str(int(Alfa2[1,0]))+" ± "+str(int(Alfa2[1,1])),str(ebeta2[1,0])+" ± "+str(ebeta2[1,1]),str(int(P2[1,0]))+" ± "+str(int(P2[1,1]))],
                         ['Cruzada', int(BW[1,0]),'-','-','-',str(int(Alfa12[1,0]))+" ± "+str(int(Alfa12[1,1])),'-',str(int(P12[1,0]))+" ± "+str(int(P12[1,1]))],
                         ['CIC 1', int(BW[2,0]),str(I1[2,0])+" ± "+str(I1[2,1]),str(ICM4[2,0])+" ± "+str(ICM4[2,1]),str(Fq1[2,0])+" ± "+str(Fq1[2,1]),str(int(Alfa1[2,0]))+" ± "+str(int(Alfa1[2,1])),str(ebeta1[2,0])+" ± "+str(ebeta1[2,1]),str(int(P1[2,0]))+" ± "+str(int(P1[2,1]))],
                         ['CIC 2', int(BW[2,0]),str(I2[2,0])+" ± "+str(I2[2,1]),str(ICM4[2,0])+" ± "+str(ICM4[2,1]),str(Fq2[2,0])+" ± "+str(Fq2[2,1]),str(int(Alfa2[2,0]))+" ± "+str(int(Alfa2[2,1])),str(ebeta2[2,0])+" ± "+str(ebeta2[2,1]),str(int(P2[2,0]))+" ± "+str(int(P2[2,1]))],
                         ['Cruzada', int(BW[2,0]),'-','-','-',str(int(Alfa12[2,0]))+" ± "+str(int(Alfa12[2,1])),'-',str(int(P12[2,0]))+" ± "+str(int(P12[2,1]))],
                         ['CIC 1', int(BW[3,0]),str(I1[3,0])+" ± "+str(I1[3,1]),str(ICM4[3,0])+" ± "+str(ICM4[3,1]),str(Fq1[3,0])+" ± "+str(Fq1[3,1]),str(int(Alfa1[3,0]))+" ± "+str(int(Alfa1[3,1])),str(ebeta1[3,0])+" ± "+str(ebeta1[3,1]),str(int(P1[3,0]))+" ± "+str(int(P1[3,1]))],
                         ['CIC 2', int(BW[3,0]),str(I2[3,0])+" ± "+str(I2[3,1]),str(ICM4[3,0])+" ± "+str(ICM4[3,1]),str(Fq2[3,0])+" ± "+str(Fq2[3,1]),str(int(Alfa2[3,0]))+" ± "+str(int(Alfa2[3,1])),str(ebeta2[3,0])+" ± "+str(ebeta2[3,1]),str(int(P2[3,0]))+" ± "+str(int(P2[3,1]))],
                         ['Cruzada', int(BW[3,0]),'-','-','-',str(int(Alfa12[3,0]))+" ± "+str(int(Alfa12[3,1])),'-',str(int(P12[3,0]))+" ± "+str(int(P12[3,1]))],
                         ['CIC 1', int(BW[4,0]),str(I1[4,0])+" ± "+str(I1[4,1]),str(ICM4[4,0])+" ± "+str(ICM4[4,1]),str(Fq1[4,0])+" ± "+str(Fq1[4,1]),str(int(Alfa1[4,0]))+" ± "+str(int(Alfa1[4,1])),str(ebeta1[4,0])+" ± "+str(ebeta1[4,1]),str(int(P1[4,0]))+" ± "+str(int(P1[4,1]))],
                         ['CIC 2', int(BW[4,0]),str(I2[4,0])+" ± "+str(I2[4,1]),str(ICM4[4,0])+" ± "+str(ICM4[4,1]),str(Fq2[4,0])+" ± "+str(Fq2[4,1]),str(int(Alfa2[4,0]))+" ± "+str(int(Alfa2[4,1])),str(ebeta2[4,0])+" ± "+str(ebeta2[4,1]),str(int(P2[4,0]))+" ± "+str(int(P2[4,1]))],
                         ['Cruzada', int(BW[4,0]),'-','-','-',str(int(Alfa12[4,0]))+" ± "+str(int(Alfa12[4,1])),'-',str(int(P12[4,0]))+" ± "+str(int(P12[4,1]))],
                         ['CIC 1', int(BW[5,0]),str(I1[5,0])+" ± "+str(I1[5,1]),str(ICM4[5,0])+" ± "+str(ICM4[5,1]),str(Fq1[5,0])+" ± "+str(Fq1[5,1]),str(int(Alfa1[5,0]))+" ± "+str(int(Alfa1[5,1])),str(ebeta1[5,0])+" ± "+str(ebeta1[5,1]),str(int(P1[5,0]))+" ± "+str(int(P1[5,1]))],
                         ['CIC 2', int(BW[5,0]),str(I2[5,0])+" ± "+str(I2[5,1]),str(ICM4[5,0])+" ± "+str(ICM4[5,1]),str(Fq2[5,0])+" ± "+str(Fq2[5,1]),str(int(Alfa2[5,0]))+" ± "+str(int(Alfa2[5,1])),str(ebeta2[5,0])+" ± "+str(ebeta2[5,1]),str(int(P2[5,0]))+" ± "+str(int(P2[5,1]))],
                         ['Cruzada', int(BW[5,0]),'-','-','-',str(int(Alfa12[5,0]))+" ± "+str(int(Alfa12[5,1])),'-',str(int(P12[5,0]))+" ± "+str(int(P12[5,1]))],
                         ['CIC 1', int(BW[6,0]),str(I1[6,0])+" ± "+str(I1[6,1]),str(ICM4[6,0])+" ± "+str(ICM4[6,1]),str(Fq1[6,0])+" ± "+str(Fq1[6,1]),str(int(Alfa1[6,0]))+" ± "+str(int(Alfa1[6,1])),str(ebeta1[6,0])+" ± "+str(ebeta1[6,1]),str(int(P1[6,0]))+" ± "+str(int(P1[6,1]))],
                         ['CIC 2', int(BW[6,0]),str(I2[6,0])+" ± "+str(I2[6,1]),str(ICM4[6,0])+" ± "+str(ICM4[6,1]),str(Fq2[6,0])+" ± "+str(Fq2[6,1]),str(int(Alfa2[6,0]))+" ± "+str(int(Alfa2[6,1])),str(ebeta2[6,0])+" ± "+str(ebeta2[6,1]),str(int(P2[6,0]))+" ± "+str(int(P2[6,1]))],
                         ['Cruzada', int(BW[6,0]),'-','-','-',str(int(Alfa12[6,0]))+" ± "+str(int(Alfa12[6,1])),'-',str(int(P12[6,0]))+" ± "+str(int(P12[6,1]))],
                         ['CIC 1', int(BW[7,0]),str(I1[7,0])+" ± "+str(I1[7,1]),str(ICM4[7,0])+" ± "+str(ICM4[7,1]),str(Fq1[7,0])+" ± "+str(Fq1[7,1]),str(int(Alfa1[7,0]))+" ± "+str(int(Alfa1[7,1])),str(ebeta1[7,0])+" ± "+str(ebeta1[7,1]),str(int(P1[7,0]))+" ± "+str(int(P1[7,1]))],
                         ['CIC 2', int(BW[7,0]),str(I2[7,0])+" ± "+str(I2[7,1]),str(ICM4[7,0])+" ± "+str(ICM4[7,1]),str(Fq2[7,0])+" ± "+str(Fq2[7,1]),str(int(Alfa2[7,0]))+" ± "+str(int(Alfa2[7,1])),str(ebeta2[7,0])+" ± "+str(ebeta2[7,1]),str(int(P2[7,0]))+" ± "+str(int(P2[7,1]))],
                         ['Cruzada', int(BW[7,0]),'-','-','-',str(int(Alfa12[7,0]))+" ± "+str(int(Alfa12[7,1])),'-',str(int(P12[7,0]))+" ± "+str(int(P12[7,1]))],
                         ]
                table=Table(datad,[50,30,90,90,90,50,50,80])

                table.setStyle(TableStyle([    
                             ('BACKGROUND', (0,0), (9,0), colors.lightgrey),
                             ('BOX', (0,0), (-1,-1),0.5, colors.black),
                             ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                             ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                             ('VALIGN',(0,-1),(-1,-1),'MIDDLE'),
                             ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                             ('FONTSIZE',(0,0),(-1,-1),7),
                             ('ALIGN', (0,0), (-1,-1), 'CENTER')
                             ]))

                table.wrapOn(c, 10,90)
                table.drawOn(c, 40,210)

            elif (CantRow==9):

                datad=[
                         ['Detector', 'BW (Hz)', 'I (A)', 'CM4 (A)', 'fq (C)', 'α (Hz)' , ' ε/β^2','Potencia (W)'],
                         ['CIC 1', int(BW[0,0]),str(I1[0,0])+" ± "+str(I1[0,1]),str(ICM4[0,0])+" ± "+str(ICM4[0,1]),str(Fq1[0,0])+" ± "+str(Fq1[0,1]),str(int(Alfa1[0,0]))+" ± "+str(int(Alfa1[0,1])),str(ebeta1[0,0])+" ± "+str(ebeta1[0,1]),str(int(P1[0,0]))+" ± "+str(int(P1[0,1]))],
                         ['CIC 2', int(BW[0,0]),str(I2[0,0])+" ± "+str(I2[0,1]),str(ICM4[0,0])+" ± "+str(ICM4[0,1]),str(Fq2[0,0])+" ± "+str(Fq2[0,1]),str(int(Alfa2[0,0]))+" ± "+str(int(Alfa2[0,1])),str(ebeta2[0,0])+" ± "+str(ebeta2[0,1]),str(int(P2[0,0]))+" ± "+str(int(P2[0,1]))],
                         ['Cruzada', int(BW[0,0]),'-','-','-',str(int(Alfa12[0,0]))+" ± "+str(int(Alfa12[0,1])),'-',str(int(P12[0,0]))+" ± "+str(int(P12[0,1]))],
                         ['CIC 1', int(BW[1,0]),str(I1[1,0])+" ± "+str(I1[1,1]),str(ICM4[1,0])+" ± "+str(ICM4[1,1]),str(Fq1[1,0])+" ± "+str(Fq1[1,1]),str(int(Alfa1[1,0]))+" ± "+str(int(Alfa1[1,1])),str(ebeta1[1,0])+" ± "+str(ebeta1[1,1]),str(int(P1[1,0]))+" ± "+str(int(P1[1,1]))],
                         ['CIC 2', int(BW[1,0]),str(I2[1,0])+" ± "+str(I2[1,1]),str(ICM4[1,0])+" ± "+str(ICM4[1,1]),str(Fq2[1,0])+" ± "+str(Fq2[1,1]),str(int(Alfa2[1,0]))+" ± "+str(int(Alfa2[1,1])),str(ebeta2[1,0])+" ± "+str(ebeta2[1,1]),str(int(P2[1,0]))+" ± "+str(int(P2[1,1]))],
                         ['Cruzada', int(BW[1,0]),'-','-','-',str(int(Alfa12[1,0]))+" ± "+str(int(Alfa12[1,1])),'-',str(int(P12[1,0]))+" ± "+str(int(P12[1,1]))],
                         ['CIC 1', int(BW[2,0]),str(I1[2,0])+" ± "+str(I1[2,1]),str(ICM4[2,0])+" ± "+str(ICM4[2,1]),str(Fq1[2,0])+" ± "+str(Fq1[2,1]),str(int(Alfa1[2,0]))+" ± "+str(int(Alfa1[2,1])),str(ebeta1[2,0])+" ± "+str(ebeta1[2,1]),str(int(P1[2,0]))+" ± "+str(int(P1[2,1]))],
                         ['CIC 2', int(BW[2,0]),str(I2[2,0])+" ± "+str(I2[2,1]),str(ICM4[2,0])+" ± "+str(ICM4[2,1]),str(Fq2[2,0])+" ± "+str(Fq2[2,1]),str(int(Alfa2[2,0]))+" ± "+str(int(Alfa2[2,1])),str(ebeta2[2,0])+" ± "+str(ebeta2[2,1]),str(int(P2[2,0]))+" ± "+str(int(P2[2,1]))],
                         ['Cruzada', int(BW[2,0]),'-','-','-',str(int(Alfa12[2,0]))+" ± "+str(int(Alfa12[2,1])),'-',str(int(P12[2,0]))+" ± "+str(int(P12[2,1]))],
                         ['CIC 1', int(BW[3,0]),str(I1[3,0])+" ± "+str(I1[3,1]),str(ICM4[3,0])+" ± "+str(ICM4[3,1]),str(Fq1[3,0])+" ± "+str(Fq1[3,1]),str(int(Alfa1[3,0]))+" ± "+str(int(Alfa1[3,1])),str(ebeta1[3,0])+" ± "+str(ebeta1[3,1]),str(int(P1[3,0]))+" ± "+str(int(P1[3,1]))],
                         ['CIC 2', int(BW[3,0]),str(I2[3,0])+" ± "+str(I2[3,1]),str(ICM4[3,0])+" ± "+str(ICM4[3,1]),str(Fq2[3,0])+" ± "+str(Fq2[3,1]),str(int(Alfa2[3,0]))+" ± "+str(int(Alfa2[3,1])),str(ebeta2[3,0])+" ± "+str(ebeta2[3,1]),str(int(P2[3,0]))+" ± "+str(int(P2[3,1]))],
                         ['Cruzada', int(BW[3,0]),'-','-','-',str(int(Alfa12[3,0]))+" ± "+str(int(Alfa12[3,1])),'-',str(int(P12[3,0]))+" ± "+str(int(P12[3,1]))],
                         ['CIC 1', int(BW[4,0]),str(I1[4,0])+" ± "+str(I1[4,1]),str(ICM4[4,0])+" ± "+str(ICM4[4,1]),str(Fq1[4,0])+" ± "+str(Fq1[4,1]),str(int(Alfa1[4,0]))+" ± "+str(int(Alfa1[4,1])),str(ebeta1[4,0])+" ± "+str(ebeta1[4,1]),str(int(P1[4,0]))+" ± "+str(int(P1[4,1]))],
                         ['CIC 2', int(BW[4,0]),str(I2[4,0])+" ± "+str(I2[4,1]),str(ICM4[4,0])+" ± "+str(ICM4[4,1]),str(Fq2[4,0])+" ± "+str(Fq2[4,1]),str(int(Alfa2[4,0]))+" ± "+str(int(Alfa2[4,1])),str(ebeta2[4,0])+" ± "+str(ebeta2[4,1]),str(int(P2[4,0]))+" ± "+str(int(P2[4,1]))],
                         ['Cruzada', int(BW[4,0]),'-','-','-',str(int(Alfa12[4,0]))+" ± "+str(int(Alfa12[4,1])),'-',str(int(P12[4,0]))+" ± "+str(int(P12[4,1]))],
                         ['CIC 1', int(BW[5,0]),str(I1[5,0])+" ± "+str(I1[5,1]),str(ICM4[5,0])+" ± "+str(ICM4[5,1]),str(Fq1[5,0])+" ± "+str(Fq1[5,1]),str(int(Alfa1[5,0]))+" ± "+str(int(Alfa1[5,1])),str(ebeta1[5,0])+" ± "+str(ebeta1[5,1]),str(int(P1[5,0]))+" ± "+str(int(P1[5,1]))],
                         ['CIC 2', int(BW[5,0]),str(I2[5,0])+" ± "+str(I2[5,1]),str(ICM4[5,0])+" ± "+str(ICM4[5,1]),str(Fq2[5,0])+" ± "+str(Fq2[5,1]),str(int(Alfa2[5,0]))+" ± "+str(int(Alfa2[5,1])),str(ebeta2[5,0])+" ± "+str(ebeta2[5,1]),str(int(P2[5,0]))+" ± "+str(int(P2[5,1]))],
                         ['Cruzada', int(BW[5,0]),'-','-','-',str(int(Alfa12[5,0]))+" ± "+str(int(Alfa12[5,1])),'-',str(int(P12[5,0]))+" ± "+str(int(P12[5,1]))],
                         ['CIC 1', int(BW[6,0]),str(I1[6,0])+" ± "+str(I1[6,1]),str(ICM4[6,0])+" ± "+str(ICM4[6,1]),str(Fq1[6,0])+" ± "+str(Fq1[6,1]),str(int(Alfa1[6,0]))+" ± "+str(int(Alfa1[6,1])),str(ebeta1[6,0])+" ± "+str(ebeta1[6,1]),str(int(P1[6,0]))+" ± "+str(int(P1[6,1]))],
                         ['CIC 2', int(BW[6,0]),str(I2[6,0])+" ± "+str(I2[6,1]),str(ICM4[6,0])+" ± "+str(ICM4[6,1]),str(Fq2[6,0])+" ± "+str(Fq2[6,1]),str(int(Alfa2[6,0]))+" ± "+str(int(Alfa2[6,1])),str(ebeta2[6,0])+" ± "+str(ebeta2[6,1]),str(int(P2[6,0]))+" ± "+str(int(P2[6,1]))],
                         ['Cruzada', int(BW[6,0]),'-','-','-',str(int(Alfa12[6,0]))+" ± "+str(int(Alfa12[6,1])),'-',str(int(P12[6,0]))+" ± "+str(int(P12[6,1]))],
                         ['CIC 1', int(BW[7,0]),str(I1[7,0])+" ± "+str(I1[7,1]),str(ICM4[7,0])+" ± "+str(ICM4[7,1]),str(Fq1[7,0])+" ± "+str(Fq1[7,1]),str(int(Alfa1[7,0]))+" ± "+str(int(Alfa1[7,1])),str(ebeta1[7,0])+" ± "+str(ebeta1[7,1]),str(int(P1[7,0]))+" ± "+str(int(P1[7,1]))],
                         ['CIC 2', int(BW[7,0]),str(I2[7,0])+" ± "+str(I2[7,1]),str(ICM4[7,0])+" ± "+str(ICM4[7,1]),str(Fq2[7,0])+" ± "+str(Fq2[7,1]),str(int(Alfa2[7,0]))+" ± "+str(int(Alfa2[7,1])),str(ebeta2[7,0])+" ± "+str(ebeta2[7,1]),str(int(P2[7,0]))+" ± "+str(int(P2[7,1]))],
                         ['Cruzada', int(BW[7,0]),'-','-','-',str(int(Alfa12[7,0]))+" ± "+str(int(Alfa12[7,1])),'-',str(int(P12[7,0]))+" ± "+str(int(P12[7,1]))],
                         ['CIC 1', int(BW[8,0]),str(I1[8,0])+" ± "+str(I1[8,1]),str(ICM4[8,0])+" ± "+str(ICM4[8,1]),str(Fq1[8,0])+" ± "+str(Fq1[8,1]),str(int(Alfa1[8,0]))+" ± "+str(int(Alfa1[8,1])),str(ebeta1[8,0])+" ± "+str(ebeta1[8,1]),str(int(P1[8,0]))+" ± "+str(int(P1[8,1]))],
                         ['CIC 2', int(BW[8,0]),str(I2[8,0])+" ± "+str(I2[8,1]),str(ICM4[8,0])+" ± "+str(ICM4[8,1]),str(Fq2[8,0])+" ± "+str(Fq2[8,1]),str(int(Alfa2[8,0]))+" ± "+str(int(Alfa2[8,1])),str(ebeta2[8,0])+" ± "+str(ebeta2[8,1]),str(int(P2[8,0]))+" ± "+str(int(P2[8,1]))],
                         ['Cruzada', int(BW[8,0]),'-','-','-',str(int(Alfa12[8,0]))+" ± "+str(int(Alfa12[8,1])),'-',str(int(P12[8,0]))+" ± "+str(int(P12[8,1]))],
                         ]
                table=Table(datad,[50,30,90,90,90,50,50,80])

                table.setStyle(TableStyle([    
                             ('BACKGROUND', (0,0), (9,0), colors.lightgrey),
                             ('BOX', (0,0), (-1,-1),0.5, colors.black),
                             ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                             ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                             ('VALIGN',(0,-1),(-1,-1),'MIDDLE'),
                             ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                             ('FONTSIZE',(0,0),(-1,-1),7),
                             ('ALIGN', (0,0), (-1,-1), 'CENTER')
                             ]))

                table.wrapOn(c, 10,90)
                table.drawOn(c, 40,156)


            elif (CantRow==10):

                datad=[
                         ['Detector', 'BW (Hz)', 'I (A)', 'CM4 (A)', 'fq (C)', 'α (Hz)' , ' ε/β^2','Potencia (W)'],
                         ['CIC 1', int(BW[0,0]),str(I1[0,0])+" ± "+str(I1[0,1]),str(ICM4[0,0])+" ± "+str(ICM4[0,1]),str(Fq1[0,0])+" ± "+str(Fq1[0,1]),str(int(Alfa1[0,0]))+" ± "+str(int(Alfa1[0,1])),str(ebeta1[0,0])+" ± "+str(ebeta1[0,1]),str(int(P1[0,0]))+" ± "+str(int(P1[0,1]))],
                         ['CIC 2', int(BW[0,0]),str(I2[0,0])+" ± "+str(I2[0,1]),str(ICM4[0,0])+" ± "+str(ICM4[0,1]),str(Fq2[0,0])+" ± "+str(Fq2[0,1]),str(int(Alfa2[0,0]))+" ± "+str(int(Alfa2[0,1])),str(ebeta2[0,0])+" ± "+str(ebeta2[0,1]),str(int(P2[0,0]))+" ± "+str(int(P2[0,1]))],
                         ['Cruzada', int(BW[0,0]),'-','-','-',str(int(Alfa12[0,0]))+" ± "+str(int(Alfa12[0,1])),'-',str(int(P12[0,0]))+" ± "+str(int(P12[0,1]))],
                         ['CIC 1', int(BW[1,0]),str(I1[1,0])+" ± "+str(I1[1,1]),str(ICM4[1,0])+" ± "+str(ICM4[1,1]),str(Fq1[1,0])+" ± "+str(Fq1[1,1]),str(int(Alfa1[1,0]))+" ± "+str(int(Alfa1[1,1])),str(ebeta1[1,0])+" ± "+str(ebeta1[1,1]),str(int(P1[1,0]))+" ± "+str(int(P1[1,1]))],
                         ['CIC 2', int(BW[1,0]),str(I2[1,0])+" ± "+str(I2[1,1]),str(ICM4[1,0])+" ± "+str(ICM4[1,1]),str(Fq2[1,0])+" ± "+str(Fq2[1,1]),str(int(Alfa2[1,0]))+" ± "+str(int(Alfa2[1,1])),str(ebeta2[1,0])+" ± "+str(ebeta2[1,1]),str(int(P2[1,0]))+" ± "+str(int(P2[1,1]))],
                         ['Cruzada', int(BW[1,0]),'-','-','-',str(int(Alfa12[1,0]))+" ± "+str(int(Alfa12[1,1])),'-',str(int(P12[1,0]))+" ± "+str(int(P12[1,1]))],
                         ['CIC 1', int(BW[2,0]),str(I1[2,0])+" ± "+str(I1[2,1]),str(ICM4[2,0])+" ± "+str(ICM4[2,1]),str(Fq1[2,0])+" ± "+str(Fq1[2,1]),str(int(Alfa1[2,0]))+" ± "+str(int(Alfa1[2,1])),str(ebeta1[2,0])+" ± "+str(ebeta1[2,1]),str(int(P1[2,0]))+" ± "+str(int(P1[2,1]))],
                         ['CIC 2', int(BW[2,0]),str(I2[2,0])+" ± "+str(I2[2,1]),str(ICM4[2,0])+" ± "+str(ICM4[2,1]),str(Fq2[2,0])+" ± "+str(Fq2[2,1]),str(int(Alfa2[2,0]))+" ± "+str(int(Alfa2[2,1])),str(ebeta2[2,0])+" ± "+str(ebeta2[2,1]),str(int(P2[2,0]))+" ± "+str(int(P2[2,1]))],
                         ['Cruzada', int(BW[2,0]),'-','-','-',str(int(Alfa12[2,0]))+" ± "+str(int(Alfa12[2,1])),'-',str(int(P12[2,0]))+" ± "+str(int(P12[2,1]))],
                         ['CIC 1', int(BW[3,0]),str(I1[3,0])+" ± "+str(I1[3,1]),str(ICM4[3,0])+" ± "+str(ICM4[3,1]),str(Fq1[3,0])+" ± "+str(Fq1[3,1]),str(int(Alfa1[3,0]))+" ± "+str(int(Alfa1[3,1])),str(ebeta1[3,0])+" ± "+str(ebeta1[3,1]),str(int(P1[3,0]))+" ± "+str(int(P1[3,1]))],
                         ['CIC 2', int(BW[3,0]),str(I2[3,0])+" ± "+str(I2[3,1]),str(ICM4[3,0])+" ± "+str(ICM4[3,1]),str(Fq2[3,0])+" ± "+str(Fq2[3,1]),str(int(Alfa2[3,0]))+" ± "+str(int(Alfa2[3,1])),str(ebeta2[3,0])+" ± "+str(ebeta2[3,1]),str(int(P2[3,0]))+" ± "+str(int(P2[3,1]))],
                         ['Cruzada', int(BW[3,0]),'-','-','-',str(int(Alfa12[3,0]))+" ± "+str(int(Alfa12[3,1])),'-',str(int(P12[3,0]))+" ± "+str(int(P12[3,1]))],
                         ['CIC 1', int(BW[4,0]),str(I1[4,0])+" ± "+str(I1[4,1]),str(ICM4[4,0])+" ± "+str(ICM4[4,1]),str(Fq1[4,0])+" ± "+str(Fq1[4,1]),str(int(Alfa1[4,0]))+" ± "+str(int(Alfa1[4,1])),str(ebeta1[4,0])+" ± "+str(ebeta1[4,1]),str(int(P1[4,0]))+" ± "+str(int(P1[4,1]))],
                         ['CIC 2', int(BW[4,0]),str(I2[4,0])+" ± "+str(I2[4,1]),str(ICM4[4,0])+" ± "+str(ICM4[4,1]),str(Fq2[4,0])+" ± "+str(Fq2[4,1]),str(int(Alfa2[4,0]))+" ± "+str(int(Alfa2[4,1])),str(ebeta2[4,0])+" ± "+str(ebeta2[4,1]),str(int(P2[4,0]))+" ± "+str(int(P2[4,1]))],
                         ['Cruzada', int(BW[4,0]),'-','-','-',str(int(Alfa12[4,0]))+" ± "+str(int(Alfa12[4,1])),'-',str(int(P12[4,0]))+" ± "+str(int(P12[4,1]))],
                         ['CIC 1', int(BW[5,0]),str(I1[5,0])+" ± "+str(I1[5,1]),str(ICM4[5,0])+" ± "+str(ICM4[5,1]),str(Fq1[5,0])+" ± "+str(Fq1[5,1]),str(int(Alfa1[5,0]))+" ± "+str(int(Alfa1[5,1])),str(ebeta1[5,0])+" ± "+str(ebeta1[5,1]),str(int(P1[5,0]))+" ± "+str(int(P1[5,1]))],
                         ['CIC 2', int(BW[5,0]),str(I2[5,0])+" ± "+str(I2[5,1]),str(ICM4[5,0])+" ± "+str(ICM4[5,1]),str(Fq2[5,0])+" ± "+str(Fq2[5,1]),str(int(Alfa2[5,0]))+" ± "+str(int(Alfa2[5,1])),str(ebeta2[5,0])+" ± "+str(ebeta2[5,1]),str(int(P2[5,0]))+" ± "+str(int(P2[5,1]))],
                         ['Cruzada', int(BW[5,0]),'-','-','-',str(int(Alfa12[5,0]))+" ± "+str(int(Alfa12[5,1])),'-',str(int(P12[5,0]))+" ± "+str(int(P12[5,1]))],
                         ['CIC 1', int(BW[6,0]),str(I1[6,0])+" ± "+str(I1[6,1]),str(ICM4[6,0])+" ± "+str(ICM4[6,1]),str(Fq1[6,0])+" ± "+str(Fq1[6,1]),str(int(Alfa1[6,0]))+" ± "+str(int(Alfa1[6,1])),str(ebeta1[6,0])+" ± "+str(ebeta1[6,1]),str(int(P1[6,0]))+" ± "+str(int(P1[6,1]))],
                         ['CIC 2', int(BW[6,0]),str(I2[6,0])+" ± "+str(I2[6,1]),str(ICM4[6,0])+" ± "+str(ICM4[6,1]),str(Fq2[6,0])+" ± "+str(Fq2[6,1]),str(int(Alfa2[6,0]))+" ± "+str(int(Alfa2[6,1])),str(ebeta2[6,0])+" ± "+str(ebeta2[6,1]),str(int(P2[6,0]))+" ± "+str(int(P2[6,1]))],
                         ['Cruzada', int(BW[6,0]),'-','-','-',str(int(Alfa12[6,0]))+" ± "+str(int(Alfa12[6,1])),'-',str(int(P12[6,0]))+" ± "+str(int(P12[6,1]))],
                         ['CIC 1', int(BW[7,0]),str(I1[7,0])+" ± "+str(I1[7,1]),str(ICM4[7,0])+" ± "+str(ICM4[7,1]),str(Fq1[7,0])+" ± "+str(Fq1[7,1]),str(int(Alfa1[7,0]))+" ± "+str(int(Alfa1[7,1])),str(ebeta1[7,0])+" ± "+str(ebeta1[7,1]),str(int(P1[7,0]))+" ± "+str(int(P1[7,1]))],
                         ['CIC 2', int(BW[7,0]),str(I2[7,0])+" ± "+str(I2[7,1]),str(ICM4[7,0])+" ± "+str(ICM4[7,1]),str(Fq2[7,0])+" ± "+str(Fq2[7,1]),str(int(Alfa2[7,0]))+" ± "+str(int(Alfa2[7,1])),str(ebeta2[7,0])+" ± "+str(ebeta2[7,1]),str(int(P2[7,0]))+" ± "+str(int(P2[7,1]))],
                         ['Cruzada', int(BW[7,0]),'-','-','-',str(int(Alfa12[7,0]))+" ± "+str(int(Alfa12[7,1])),'-',str(int(P12[7,0]))+" ± "+str(int(P12[7,1]))],
                         ['CIC 1', int(BW[8,0]),str(I1[8,0])+" ± "+str(I1[8,1]),str(ICM4[8,0])+" ± "+str(ICM4[8,1]),str(Fq1[8,0])+" ± "+str(Fq1[8,1]),str(int(Alfa1[8,0]))+" ± "+str(int(Alfa1[8,1])),str(ebeta1[8,0])+" ± "+str(ebeta1[8,1]),str(int(P1[8,0]))+" ± "+str(int(P1[8,1]))],
                         ['CIC 2', int(BW[8,0]),str(I2[8,0])+" ± "+str(I2[8,1]),str(ICM4[8,0])+" ± "+str(ICM4[8,1]),str(Fq2[8,0])+" ± "+str(Fq2[8,1]),str(int(Alfa2[8,0]))+" ± "+str(int(Alfa2[8,1])),str(ebeta2[8,0])+" ± "+str(ebeta2[8,1]),str(int(P2[8,0]))+" ± "+str(int(P2[8,1]))],
                         ['Cruzada', int(BW[8,0]),'-','-','-',str(int(Alfa12[8,0]))+" ± "+str(int(Alfa12[8,1])),'-',str(int(P12[8,0]))+" ± "+str(int(P12[8,1]))],
                         ['CIC 1', int(BW[9,0]),str(I1[9,0])+" ± "+str(I1[9,1]),str(ICM4[9,0])+" ± "+str(ICM4[9,1]),str(Fq1[9,0])+" ± "+str(Fq1[9,1]),str(int(Alfa1[9,0]))+" ± "+str(int(Alfa1[9,1])),str(ebeta1[9,0])+" ± "+str(ebeta1[9,1]),str(int(P1[9,0]))+" ± "+str(int(P1[9,1]))],
                         ['CIC 2', int(BW[9,0]),str(I2[9,0])+" ± "+str(I2[9,1]),str(ICM4[9,0])+" ± "+str(ICM4[9,1]),str(Fq2[9,0])+" ± "+str(Fq2[9,1]),str(int(Alfa2[9,0]))+" ± "+str(int(Alfa2[9,1])),str(ebeta2[9,0])+" ± "+str(ebeta2[9,1]),str(int(P2[9,0]))+" ± "+str(int(P2[9,1]))],
                         ['Cruzada', int(BW[9,0]),'-','-','-',str(int(Alfa12[9,0]))+" ± "+str(int(Alfa12[9,1])),'-',str(int(P12[9,0]))+" ± "+str(int(P12[9,1]))],
                         ]
                table=Table(datad,[50,30,90,90,90,50,50,80])

                table.setStyle(TableStyle([    
                             ('BACKGROUND', (0,0), (9,0), colors.lightgrey),
                             ('BOX', (0,0), (-1,-1),0.5, colors.black),
                             ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                             ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                             ('VALIGN',(0,-1),(-1,-1),'MIDDLE'),
                             ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                             ('FONTSIZE',(0,0),(-1,-1),7),
                             ('ALIGN', (0,0), (-1,-1), 'CENTER')
                             ]))

                table.wrapOn(c, 10,90)
                table.drawOn(c, 40,102)

            else:
                mesa='Error'
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Critical)
                msg.setText("Se procesan como mínimo tres puntos y como máximo diez.")
                msg.setInformativeText(mesa)
                msg.setWindowTitle("PotCal 1.0")
                msg.exec_()
                
            c.showPage()
            c.setFont ('Arial-Bold', tamle)
            c.drawString(50,750,"ESTIMACIÓN DE LA CONSTANTE α")
            
            if(len(Vec)==4):
                datad=[
                         ['CIC 1', 'Potencia (W)', 'α (Hz)'],
                         ['1er grupo',str(PotenciaF11)+ " ± "+str(IncPotenciaF11),str(AlfaF11)+ " ± "+str(IncAlfaF11)], 
                         ['2do grupo',str(PotenciaF21)+ " ± "+str(IncPotenciaF21),str(AlfaF21)+ " ± "+str(IncAlfaF21)],
                         ['3er grupo',str(PotenciaF31)+ " ± "+str(IncPotenciaF31),str(AlfaF31)+ " ± "+str(IncAlfaF31)],
                         ['4to grupo',str(PotenciaF41)+ " ± "+str(IncPotenciaF41),str(AlfaF41)+ " ± "+str(IncAlfaF41)],
                         
                         ]
                table=Table(datad,[70,70,70])

                print("Paolo")

                table.setStyle(TableStyle([    
                             ('BACKGROUND', (0,0), (3,0), colors.lightgrey),
                             ('BOX', (0,0), (-1,-1),0.5, colors.black),
                             ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                             ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                             ('VALIGN',(0,-1),(-1,-1),'MIDDLE'),
                             ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                             ('FONTSIZE',(0,0),(-1,-1),7),
                             ('ALIGN', (0,0), (-1,-1), 'CENTER')
                             ]))

                table.wrapOn(c, 10,90)
                table.drawOn(c, 200,640)

                datad=[
                         ['CIC 2', 'Potencia (W)', 'α (Hz)'],
                         ['1er grupo',str(PotenciaF12)+ " ± "+str(IncPotenciaF12),str(AlfaF12)+ " ± "+str(IncAlfaF12)], 
                         ['2do grupo',str(PotenciaF22)+ " ± "+str(IncPotenciaF22),str(AlfaF22)+ " ± "+str(IncAlfaF22)],
                         ['3er grupo',str(PotenciaF32)+ " ± "+str(IncPotenciaF32),str(AlfaF32)+ " ± "+str(IncAlfaF32)],
                         ['4to grupo',str(PotenciaF42)+ " ± "+str(IncPotenciaF42),str(AlfaF42)+ " ± "+str(IncAlfaF42)],
                         
                         ]
                table=Table(datad,[70,70,70])

                table.setStyle(TableStyle([    
                             ('BACKGROUND', (0,0), (3,0), colors.lightgrey),
                             ('BOX', (0,0), (-1,-1),0.5, colors.black),
                             ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                             ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                             ('VALIGN',(0,-1),(-1,-1),'MIDDLE'),
                             ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                             ('FONTSIZE',(0,0),(-1,-1),7),
                             ('ALIGN', (0,0), (-1,-1), 'CENTER')
                             ]))

                table.wrapOn(c, 10,90)
                table.drawOn(c, 200,530)

                datad=[
                         ['Cruzada', 'Potencia (W)', 'α (Hz)'],
                         ['1er grupo',str(PotenciaF1C)+ " ± "+str(IncPotenciaF1C),str(AlfaF1C)+ " ± "+str(IncAlfaF1C)], 
                         ['2do grupo',str(PotenciaF2C)+ " ± "+str(IncPotenciaF2C),str(AlfaF2C)+ " ± "+str(IncAlfaF2C)],
                         ['3er grupo',str(PotenciaF3C)+ " ± "+str(IncPotenciaF3C),str(AlfaF3C)+ " ± "+str(IncAlfaF3C)],
                         ['4to grupo',str(PotenciaF4C)+ " ± "+str(IncPotenciaF4C),str(AlfaF4C)+ " ± "+str(IncAlfaF4C)],
                         
                         ]
                table=Table(datad,[70,70,70])

                table.setStyle(TableStyle([    
                             ('BACKGROUND', (0,0), (3,0), colors.lightgrey),
                             ('BOX', (0,0), (-1,-1),0.5, colors.black),
                             ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                             ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                             ('VALIGN',(0,-1),(-1,-1),'MIDDLE'),
                             ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                             ('FONTSIZE',(0,0),(-1,-1),7),
                             ('ALIGN', (0,0), (-1,-1), 'CENTER')
                             ]))

                table.wrapOn(c, 10,90)
                table.drawOn(c, 200,420)

                plt.figure(figsize=(12,5)) ## 12,5
                Potenciagra11=[PotenciaF11, PotenciaF21, PotenciaF31, PotenciaF41]
                Potenciagra12=[PotenciaF12, PotenciaF22, PotenciaF32, PotenciaF42]
                Potenciagra1C=[PotenciaF1C, PotenciaF2C, PotenciaF3C, PotenciaF4C]
                Alfagra11=[AlfaF11, AlfaF21, AlfaF31, AlfaF41]
                Alfagra12=[AlfaF12, AlfaF22, AlfaF32, AlfaF42]
                Alfagra1C=[AlfaF1C, AlfaF2C, AlfaF3C, AlfaF4C]

                IncAlfagra11=[IncAlfaF11, IncAlfaF21, IncAlfaF31, IncAlfaF41]
                IncAlfagra12=[IncAlfaF12, IncAlfaF22, IncAlfaF32, IncAlfaF42]
                IncAlfagra1C=[IncAlfaF1C, IncAlfaF2C, IncAlfaF3C, IncAlfaF4C]

                plt.errorbar(Potenciagra11, Alfagra11, yerr=IncAlfagra11, fmt="d", color='r', lw=2 ,label="α - CIC 1")
                plt.errorbar(Potenciagra12, Alfagra12, yerr=IncAlfagra12, fmt="d", color='m', lw=2 ,label="α - CIC 2")
                plt.errorbar(Potenciagra1C, Alfagra1C, yerr=IncAlfagra1C, fmt="d", color='b', lw=2 ,label="α - Cruzada")
                Limymax=max([max(Alfagra11), max(Alfagra12), max(Alfagra1C)])+10
                Limymin=min([min(Alfagra11), min(Alfagra12), min(Alfagra1C)])-10
                grid(True)                                    
                #yticks([-round(Err*2,decim_redond), -round(3*Err/2,decim_redond), -round(Err,decim_redond), -round(Err/2,decim_redond), 0, round(Err/2,decim_redond), round(Err,decim_redond), round(3*Err/2,decim_redond), round(Err*2,decim_redond)])                    
                ylim(Limymin,Limymax)
                xlabel("Potencia (W)")
                ylabel("α ± Δα (Hz)")
    ##                    title("GRÁFICO DE INDICACIÓN VS ERROR ASCENSO (psi)")
                legend(loc='upper right')
                savefig("alfa.png")

                c.drawImage('alfa.png', 50, 220, 500, 190)

                c.setFont ('Arial', tamle)
                c.drawString(60,210,"Ponderando los valores obtenidos de α")

                datad=[
                         ['Potencia (W)', 'α (Hz)'],
                         [str(int(PotenciaPon1))+ " ± "+str(int(IncPotenciaPon1)),str(int(AlfaPon1))+ " ± "+str(int(IncAlfaPon1))], 
                         [str(int(PotenciaPon2))+ " ± "+str(int(IncPotenciaPon2)),str(int(AlfaPon2))+ " ± "+str(int(IncAlfaPon2))],
                         [str(int(PotenciaPon3))+ " ± "+str(int(IncPotenciaPon3)),str(int(AlfaPon3))+ " ± "+str(int(IncAlfaPon3))],
                         [str(int(PotenciaPon4))+ " ± "+str(int(IncPotenciaPon4)),str(int(AlfaPon4))+ " ± "+str(int(IncAlfaPon4))],
                         
                         ]
                table=Table(datad,[70,70])

                table.setStyle(TableStyle([    
                             ('BACKGROUND', (0,0), (2,0), colors.lightgrey),
                             ('BOX', (0,0), (-1,-1),0.5, colors.black),
                             ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                             ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                             ('VALIGN',(0,-1),(-1,-1),'MIDDLE'),
                             ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                             ('FONTSIZE',(0,0),(-1,-1),7),
                             ('ALIGN', (0,0), (-1,-1), 'CENTER')
                             ]))

                table.wrapOn(c, 10,90)
                table.drawOn(c, 70,100)

                
                PotenciPonagra11=[PotenciaPon1, PotenciaPon2, PotenciaPon3, PotenciaPon4]        
                AlfagraPon11=[AlfaPon1, AlfaPon2, AlfaPon3, AlfaPon4]
                IncAlfagraPon11=[IncAlfaPon1, IncAlfaPon2, IncAlfaPon3, IncAlfaPon4]

                def gunc(PotenciPonagra11,b):
                    return b
                poptalfa, pcovalfa = curve_fit(gunc,PotenciPonagra11,AlfagraPon11,sigma = IncAlfagraPon11, absolute_sigma=True)
                IncertidumbreAlfa=np.sqrt(np.diag(pcovalfa))
                residuals = AlfagraPon11- round(poptalfa[0],0)
                ss_res = np.sum(residuals**2)
                ss_tot = np.sum((AlfagraPon11-np.mean(AlfagraPon11)**2))
                Ralfa=1-np.abs((ss_res / ss_tot))
                print(Ralfa)

                plt.figure(figsize=(12,5))
                Limymax=max(AlfagraPon11)+10
                Limymin=min(AlfagraPon11)-10
                plt.errorbar(PotenciPonagra11, AlfagraPon11, yerr=IncAlfagraPon11, fmt="d", color='k', lw=2 ,label="α-i")
                axhline(round(poptalfa[0],0), color='k', xmax=1, lw=3, label="α-resultante")
                axhline(round(poptalfa[0],0)+round(IncertidumbreAlfa[0],0), color='k', xmax=1, lw=1)
                axhline(round(poptalfa[0],0)-round(IncertidumbreAlfa[0],0), color='k', xmax=1, lw=1, label="Δα")
                grid(True)                                    
                #yticks([-round(Err*2,decim_redond), -round(3*Err/2,decim_redond), -round(Err,decim_redond), -round(Err/2,decim_redond), 0, round(Err/2,decim_redond), round(Err,decim_redond), round(3*Err/2,decim_redond), round(Err*2,decim_redond)])                    
                ylim(Limymin,Limymax)
                xlabel("Potencia (W)")
                ylabel("α ± Δα (Hz)")
    ##                    title("GRÁFICO DE INDICACIÓN VS ERROR ASCENSO (psi)")
                legend(loc='upper right')
                savefig("alfapon.png")

                c.drawImage('alfapon.png', 215, 50, 400, 150)

                c.drawString(80,70," α = "+str(int(round(poptalfa[0],0)))+" ± "+str(int(round(IncertidumbreAlfa[0],0))))
                c.drawString(80,50," R = "+str(round(Ralfa,5)))
                c.drawString(87,57,"2")

                c.showPage()
                c.setFont ('Arial-Bold', tamle)
                c.drawString(50,750,"ESTIMACIÓN DE LA POTENCIA TÉRMICA")



                datad=[
                         ['CIC 1 (A)', 'Potencia (W)'],
                         [str(round(CIC11,self.decimfI1+1))+ " ± "+str(round(IncCIC11,self.decimfI1+1)),str(PotenciaF11)+ " ± "+str(IncPotenciaF11)], 
                         [str(round(CIC21,self.decimfI1))+ " ± "+str(round(IncCIC21,self.decimfI1)),str(PotenciaF21)+ " ± "+str(IncPotenciaF21)],
                         [str(round(CIC31,self.decimfI1))+ " ± "+str(round(IncCIC31,self.decimfI1)),str(PotenciaF31)+ " ± "+str(IncPotenciaF31)],
                         [str(round(CIC41,self.decimfI1))+ " ± "+str(round(IncCIC41,self.decimfI1)),str(PotenciaF41)+ " ± "+str(IncPotenciaF41)],
                         
                         ]
                table=Table(datad,[100,60])

                table.setStyle(TableStyle([    
                             ('BACKGROUND', (0,0), (2,0), colors.lightgrey),
                             ('BOX', (0,0), (-1,-1),0.5, colors.black),
                             ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                             ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                             ('VALIGN',(0,-1),(-1,-1),'MIDDLE'),
                             ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                             ('FONTSIZE',(0,0),(-1,-1),7),
                             ('ALIGN', (0,0), (-1,-1), 'CENTER')
                             ]))

                table.wrapOn(c, 10,90)
                table.drawOn(c, 50,640)


                CICFF1=np.array([CIC11,CIC21,CIC31,CIC41])
                PotenciaFF1=np.array([PotenciaF11,PotenciaF21,PotenciaF31,PotenciaF41])
                IncPotenciaFF1=np.array([IncPotenciaF11,IncPotenciaF21,IncPotenciaF31,IncPotenciaF41])
                #----- borrar -----#
                #PotenciaFF1=np.array([154, 471, 857, 1708])
                #IncPotenciaFF1=np.array([3, 12, 19, 37])
                #----- ------ -----#
                def gun2(CICFF1,a,b):
                    return a*CICFF1+b
                poptPotC1, pcovPotC1 = curve_fit(gun2,CICFF1,PotenciaFF1,sigma = IncPotenciaFF1, absolute_sigma=True)
                residuals = PotenciaFF1-gun2(CICFF1, *poptPotC1)

                ss_res = np.sum(residuals**2)
                ss_tot = np.sum((PotenciaFF1-np.mean(PotenciaFF1))**2)
                RPotC1 = 1 - np.abs(ss_res / ss_tot)

                print(RPotC1)
                plt.figure(figsize=(12,5))                
                plt.plot(CICFF1,gun2(CICFF1, *poptPotC1), color='b')
                plt.errorbar(CICFF1, PotenciaFF1, yerr=IncPotenciaFF1, fmt="d", color='r', lw=2 ,label="Pot-i")
                grid(True)                                    
                xlabel("CIC 1 (A)")
                ylabel("Potencia (W)")
                legend(loc='lower right')
                savefig("Potencia1.png")
                IncPotFF1=np.sqrt(np.diag(pcovPotC1))
                c.drawImage('Potencia1.png', 215, 600, 400, 150)
                c.drawString(270,720,"P(I) = a*I(A) + b")
                c.setFont ('Arial', tamle)
                c.drawString(270,700,"a = "+str(np.format_float_scientific(poptPotC1[0], unique = False, precision=5))+" ± "+str(np.format_float_scientific(IncPotFF1[0], unique = False, precision=5)))
                c.drawString(270,688,"b = "+str(np.format_float_scientific(poptPotC1[1], unique = False, precision=5))+" ± "+str(np.format_float_scientific(IncPotFF1[1], unique = False, precision=5)))
                c.drawString(270,676,"R = "+str(round(RPotC1,5)))
                c.drawString(276,681,"2")
                
                c.drawString(70,610,"dPot")
                c.line(68,608,98,608)
                c.drawString(70,600,"dICIC1")
                c.drawString(105,607,"= "+str(np.format_float_scientific(poptPotC1[0], unique = False, precision=5))+" ± "+str(np.format_float_scientific(IncPotFF1[0], unique = False, precision=5))+" W/A")


                datad=[
                         ['CIC 2 (A)', 'Potencia (W)'],
                         [str(round(CIC12,self.decimfI2+1))+ " ± "+str(round(IncCIC12,self.decimfI2+1)),str(PotenciaF12)+ " ± "+str(IncPotenciaF12)], 
                         [str(round(CIC22,self.decimfI2))+ " ± "+str(round(IncCIC22,self.decimfI2)),str(PotenciaF22)+ " ± "+str(IncPotenciaF22)],
                         [str(round(CIC32,self.decimfI2))+ " ± "+str(round(IncCIC32,self.decimfI2)),str(PotenciaF32)+ " ± "+str(IncPotenciaF32)],
                         [str(round(CIC42,self.decimfI2))+ " ± "+str(round(IncCIC42,self.decimfI2)),str(PotenciaF42)+ " ± "+str(IncPotenciaF42)],
                         
                         ]
                table=Table(datad,[100,60])

                table.setStyle(TableStyle([    
                             ('BACKGROUND', (0,0), (2,0), colors.lightgrey),
                             ('BOX', (0,0), (-1,-1),0.5, colors.black),
                             ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                             ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                             ('VALIGN',(0,-1),(-1,-1),'MIDDLE'),
                             ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                             ('FONTSIZE',(0,0),(-1,-1),7),
                             ('ALIGN', (0,0), (-1,-1), 'CENTER')
                             ]))

                table.wrapOn(c, 10,90)
                table.drawOn(c, 50,480)

                CICFF2=np.array([CIC12,CIC22,CIC32,CIC42])
                PotenciaFF2=np.array([PotenciaF12,PotenciaF22,PotenciaF32,PotenciaF42])
                IncPotenciaFF2=np.array([IncPotenciaF12,IncPotenciaF22,IncPotenciaF32,IncPotenciaF42])
                #----- borrar -----#
                #PotenciaFF2=np.array([157, 447, 788, 1569])
                #IncPotenciaFF2=np.array([3, 11, 16, 33])
                #----- ------ -----#
                def gun2(CICFF2,a,b):
                    return a*CICFF2+b
                poptPotC2, pcovPotC2 = curve_fit(gun2,CICFF2,PotenciaFF2,sigma = IncPotenciaFF2, absolute_sigma=True)
                residuals = PotenciaFF2-gun2(CICFF2, *poptPotC2)

                ss_res = np.sum(residuals**2)
                ss_tot = np.sum((PotenciaFF2-np.mean(PotenciaFF2))**2)
                RPotC2 = 1 - np.abs(ss_res / ss_tot)

                print(RPotC2)
                plt.figure(figsize=(12,5))                
                plt.plot(CICFF2,gun2(CICFF2, *poptPotC2), color='b')
                plt.errorbar(CICFF2, PotenciaFF2, yerr=IncPotenciaFF2, fmt="d", color='r', lw=2 ,label="Pot-i")
                grid(True)                                    
                xlabel("CIC 2 (A)")
                ylabel("Potencia (W)")
    ##                    title("GRÁFICO DE INDICACIÓN VS ERROR ASCENSO (psi)")
                legend(loc='lower right')
                savefig("Potencia2.png")
                IncPotFF2=np.sqrt(np.diag(pcovPotC2))
                c.drawImage('Potencia2.png', 215, 450, 400, 150)

                c.setFont ('Arial-Bold', tamle)
                c.drawString(270,570,"P(I) = a*I(A) + b")
                c.setFont ('Arial', tamle)
                c.drawString(270,550,"a = "+str(np.format_float_scientific(poptPotC2[0], unique = False, precision=5))+" ± "+str(np.format_float_scientific(IncPotFF2[0], unique = False, precision=5)))
                c.drawString(270,538,"b = "+str(np.format_float_scientific(poptPotC2[1], unique = False, precision=5))+" ± "+str(np.format_float_scientific(IncPotFF2[1], unique = False, precision=5)))
                c.drawString(270,526,"R = "+str(round(RPotC2,5)))
                c.drawString(276,531,"2")
                
                c.drawString(70,460,"dPot")
                c.line(68,458,98,458)
                c.drawString(70,450,"dICIC2")
                c.drawString(105,457,"= "+str(np.format_float_scientific(poptPotC2[0], unique = False, precision=5))+" ± "+str(np.format_float_scientific(IncPotFF2[0], unique = False, precision=5))+" W/A")


                datad=[
                         ['ICM4 (A)', 'Potencia (W)'],
                         [str(round(CM411,self.decimCM4f))+ " ± "+str(round(IncCM411,self.decimCM4f)),str(PotenciaF1C)+ " ± "+str(IncPotenciaF1C)], 
                         [str(round(CM421,self.decimCM4f))+ " ± "+str(round(IncCM421,self.decimCM4f)),str(PotenciaF2C)+ " ± "+str(IncPotenciaF2C)],
                         [str(round(CM431,self.decimCM4f))+ " ± "+str(round(IncCM431,self.decimCM4f)),str(PotenciaF3C)+ " ± "+str(IncPotenciaF3C)],
                         [str(round(CM441,self.decimCM4f))+ " ± "+str(round(IncCM441,self.decimCM4f)),str(PotenciaF4C)+ " ± "+str(IncPotenciaF4C)],
                         
                         ]
                table=Table(datad,[100,60])

                table.setStyle(TableStyle([    
                             ('BACKGROUND', (0,0), (2,0), colors.lightgrey),
                             ('BOX', (0,0), (-1,-1),0.5, colors.black),
                             ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                             ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                             ('VALIGN',(0,-1),(-1,-1),'MIDDLE'),
                             ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                             ('FONTSIZE',(0,0),(-1,-1),7),
                             ('ALIGN', (0,0), (-1,-1), 'CENTER')
                             ]))

                table.wrapOn(c, 10,90)
                table.drawOn(c, 50,320)

                CM4FF=np.array([CM411,CM421,CM431,CM441])
                PotenciaFFC=np.array([PotenciaF1C,PotenciaF2C,PotenciaF3C,PotenciaF4C])
                IncPotenciaFFC=np.array([IncPotenciaF1C,IncPotenciaF2C,IncPotenciaF3C,IncPotenciaF4C])

                #----- borrar -----#
                #PotenciaFFC=np.array([159, 471, 835, 1673])
                #IncPotenciaFFC=np.array([2, 10, 14, 28])
                #----- ------ -----#

                aCM4,covCM4,RCM4=Minimos2.Cuadrados(CM4FF, PotenciaFFC, IncPotenciaFFC, 1)

                print(CM4FF)
                print(PotenciaFFC)
                print(IncPotenciaFFC)

                plt.figure(figsize=(12,5))                
                plt.plot(CM4FF, aCM4[0]+aCM4[1]*CM4FF, color='b')
                plt.errorbar(CM4FF, PotenciaFFC, yerr=IncPotenciaFFC, fmt="d", color='r', lw=2 ,label="Pot-i")
                grid(True)                                    
                xlabel("CM4 (A)")
                ylabel("Potencia (W)")
                legend(loc='lower right')
                savefig("PotenciaC.png")
                IncPotFFC=np.sqrt(np.diag(covCM4))
                c.drawImage('PotenciaC.png', 215, 300, 400, 150)

                c.setFont ('Arial-Bold', tamle)
                c.drawString(270,420,"P(I) = a*I(A) + b")
                c.setFont ('Arial', tamle)
                c.drawString(270,400,"a = "+str(np.format_float_scientific(aCM4[1], unique = False, precision=5))+" ± "+str(np.format_float_scientific(IncPotFFC[1], unique = False, precision=5)))
                c.drawString(270,388,"b = "+str(np.format_float_scientific(aCM4[0], unique = False, precision=5))+" ± "+str(np.format_float_scientific(IncPotFFC[0], unique = False, precision=5)))
                c.drawString(270,376,"R = "+str(round(RCM4,5)))
                c.drawString(276,381,"2")

                c.drawString(70,300,"dPot")
                c.line(68,298,98,298)
                c.drawString(70,290,"dICM4")
                c.drawString(105,297,"= "+str(np.format_float_scientific(aCM4[1], unique = False, precision=5))+" ± "+str(np.format_float_scientific(IncPotFFC[1], unique = False, precision=5))+" W/A")

                datad=[
                         ['ICM4 (A)', 'CIC 1 (A)', 'CIC 2 (A)'],
                         [str(round(CM411,self.decimCM4f))+ " ± "+str(round(IncCM411,self.decimCM4f)),str(round(CIC11,self.decimfI1+1))+ " ± "+str(round(IncCIC11,self.decimfI1+1)),str(round(CIC12,self.decimfI2+1))+ " ± "+str(round(IncCIC12,self.decimfI2+1))], 
                         [str(round(CM421,self.decimCM4f))+ " ± "+str(round(IncCM421,self.decimCM4f)),str(round(CIC21,self.decimfI1))+ " ± "+str(round(IncCIC21,self.decimfI1)),str(round(CIC22,self.decimfI2))+ " ± "+str(round(IncCIC22,self.decimfI2))],
                         [str(round(CM431,self.decimCM4f))+ " ± "+str(round(IncCM431,self.decimCM4f)),str(round(CIC31,self.decimfI1))+ " ± "+str(round(IncCIC31,self.decimfI1)),str(round(CIC32,self.decimfI2))+ " ± "+str(round(IncCIC32,self.decimfI2))],
                         [str(round(CM441,self.decimCM4f))+ " ± "+str(round(IncCM441,self.decimCM4f)),str(round(CIC41,self.decimfI1))+ " ± "+str(round(IncCIC41,self.decimfI1)),str(round(CIC42,self.decimfI2))+ " ± "+str(round(IncCIC42,self.decimfI2))],
                         
                         ]
                table=Table(datad,[100,100,100])

                table.setStyle(TableStyle([    
                             ('BACKGROUND', (0,0), (3,0), colors.lightgrey),
                             ('BOX', (0,0), (-1,-1),0.5, colors.black),
                             ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                             ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                             ('VALIGN',(0,-1),(-1,-1),'MIDDLE'),
                             ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                             ('FONTSIZE',(0,0),(-1,-1),7),
                             ('ALIGN', (0,0), (-1,-1), 'CENTER')
                             ]))

                table.wrapOn(c, 10,90)
                table.drawOn(c, 140,150)

                c.showPage()
                IncCICFF1=np.array([IncCIC11,IncCIC21,IncCIC31,IncCIC41])
                IncCICFF2=np.array([IncCIC12,IncCIC22,IncCIC32,IncCIC42])

                def funci(CM4FF,a,b):
                    return CM4FF*a+b

                aCIC1, covCIC1 = curve_fit(funci,CM4FF,CICFF1,sigma = IncCICFF1, absolute_sigma=True)
                aCIC2, covCIC2 = curve_fit(funci,CM4FF,CICFF2,sigma = IncCICFF2, absolute_sigma=True)
                #aCIC1e,covCIC1e,RCIC1e=Minimos2.Cuadrados(CM4FF, CICFF1, IncCICFF1, 1)
                #aCIC2e,covCIC2e,RCIC2e=Minimos2.Cuadrados(CM4FF, CICFF2, IncCICFF2, 1)

                residuals = CICFF1- funci(CM4FF, *aCIC1)
                ss_res = np.sum(residuals**2)
                ss_tot = np.sum((CICFF1-np.mean(CICFF1))**2)
                RCIC1 = 1 - (ss_res / ss_tot)

                residuals = CICFF2- funci(CM4FF, *aCIC2)
                ss_res = np.sum(residuals**2)
                ss_tot = np.sum((CICFF2-np.mean(CICFF2))**2)
                RCIC2 = 1 - (ss_res / ss_tot)
                
                plt.figure(figsize=(12,5))                
                plt.plot(CM4FF, aCIC1[1]+aCIC1[0]*CM4FF, color='k')
                plt.plot(CM4FF, aCIC2[1]+aCIC2[0]*CM4FF, color='r')
                plt.errorbar(CM4FF, CICFF1, yerr=IncCICFF1, fmt="d", color='k', lw=2 ,label="CIC 1")
                plt.errorbar(CM4FF, CICFF2, yerr=IncCICFF2, fmt="d", color='r', lw=2 ,label="CIC 2")
                grid(True)                                    
                xlabel("CM4 (A)")
                ylabel("CIC (A)")
                legend(loc='lower right')
                savefig("Corriente.png")
                IncCIC1F=np.sqrt(np.diag(covCIC1))
                IncCIC2F=np.sqrt(np.diag(covCIC2))
                c.drawImage('Corriente.png',100, 650, 400, 150)
                
                c.setFont ('Arial-Bold', tamle)
                c.drawString(100,620,"CIC1(A) = a*ICM4(A) + b")
                c.drawString(380,620,"CIC2(A) = a*ICM4(A) + b")
                c.setFont ('Arial', tamle)
                c.drawString(85,600,"a = "+str(np.format_float_scientific(aCIC1[0], unique = False, precision=5))+" ± "+str(np.format_float_scientific(IncCIC1F[0], unique = False, precision=5)))
                c.drawString(365,600,"a = "+str(np.format_float_scientific(aCIC2[0], unique = False, precision=5))+" ± "+str(np.format_float_scientific(IncCIC2F[0], unique = False, precision=5)))
                c.drawString(85,580,"b = "+str(np.format_float_scientific(aCIC1[1], unique = False, precision=5))+" ± "+str(np.format_float_scientific(IncCIC1F[1], unique = False, precision=5)))
                c.drawString(365,580,"b = "+str(np.format_float_scientific(aCIC2[1], unique = False, precision=5))+" ± "+str(np.format_float_scientific(IncCIC2F[1], unique = False, precision=5)))
                c.drawString(85,560,"R = "+str(round(RCIC1,5)))
                c.drawString(365,560,"R = "+str(round(RCIC2,5)))
                c.drawString(91,566,"2")
                c.drawString(371,566,"2")

                c.drawString(75,532,"dCIC1")
                c.line(73,530,103,530)
                c.drawString(75,522,"dICM4")
                c.drawString(105,527,"= "+str(np.format_float_scientific(aCIC1[0], unique = False, precision=5))+" ± "+str(np.format_float_scientific(IncCIC1F[0], unique = False, precision=5)))

                c.drawString(355,532,"dCIC2")
                c.line(353,530,383,530)
                c.drawString(355,522,"dICM4")
                c.drawString(385,527,"= "+str(np.format_float_scientific(aCIC2[0], unique = False, precision=5))+" ± "+str(np.format_float_scientific(IncCIC2F[0], unique = False, precision=5)))

                c.drawString(60,500,"Estimación del factor calibración del CM4.")
                c.line(150,450,440,450)
                c.drawString(165,435,"Cámara")
                c.drawString(240,435,"Relación")
                c.drawString(365,435,"Valor")
                c.line(150,425,440,425)
                c.drawString(238,410,"dP/dICIC1")
                c.drawString(300,410,str(np.format_float_scientific(poptPotC1[0], unique = False, precision=5))+" ± "+str(np.format_float_scientific(IncPotFF1[1], unique = False, precision=5))+" W/A")
                c.drawString(230,390,"dICIC1/dICM4")
                c.drawString(300,390,str(np.format_float_scientific(aCIC1[0], unique = False, precision=5))+" ± "+str(np.format_float_scientific(IncCIC1F[0], unique = False, precision=5)))
                P1_ICM4=poptPotC1[0]*aCIC1[0]
                IncP1_ICM4=P1_ICM4*np.sqrt((IncPotFF1[0]/poptPotC1[0])**2+(IncCIC1F[0]/aCIC1[0])**2)
                c.drawString(238,370,"dP/dICM4")
                c.setFont ('Arial-Bold', tamle)
                c.drawString(300,370,str(np.format_float_scientific(P1_ICM4, unique = False, precision=5))+" ± "+str(np.format_float_scientific(IncP1_ICM4, unique = False, precision=5))+" W/A")
                c.setFont ('Arial', tamle)
                c.drawString(165,390,"CIC 1")

                c.drawString(238,340,"dP/dICIC2")
                c.drawString(300,340,str(np.format_float_scientific(poptPotC2[0], unique = False, precision=5))+" ± "+str(np.format_float_scientific(IncPotFF2[0], unique = False, precision=5))+" W/A")
                c.drawString(230,320,"dICIC2/dICM4")
                c.drawString(300,320,str(np.format_float_scientific(aCIC2[0], unique = False, precision=5))+" ± "+str(np.format_float_scientific(IncCIC2F[0], unique = False, precision=5)))
                P2_ICM4=poptPotC2[0]*aCIC2[0]
                IncP2_ICM4=P2_ICM4*np.sqrt((IncPotFF2[0]/poptPotC2[0])**2+(IncCIC2F[0]/aCIC2[0])**2)
                c.drawString(238,300,"dP/dICM4")
                c.setFont ('Arial-Bold', tamle)
                c.drawString(300,300,str(np.format_float_scientific(P2_ICM4, unique = False, precision=5))+" ± "+str(np.format_float_scientific(IncP2_ICM4, unique = False, precision=5))+" W/A")
                c.setFont ('Arial', tamle)
                c.drawString(165,320,"CIC 2")

                c.drawString(238,270,"dP/dICM4")
                c.drawString(165,270,"CRUZADA")
                c.setFont ('Arial-Bold', tamle)
                c.drawString(300,270,str(np.format_float_scientific(aCM4[1], unique = False, precision=5))+" ± "+str(np.format_float_scientific(IncPotFFC[1], unique = False, precision=5))+" W/A")
                c.line(150,260,440,260)

                c.drawString(150,200,"dPot")
                c.line(148,198,178,198)
                c.drawString(150,190,"dICM4")
                c.drawString(185,197,"= ")
                P_ICM4=np.array([P1_ICM4, P2_ICM4, aCM4[1]])
                IncP_ICM4=np.array([1/IncP1_ICM4**2, 1/IncP2_ICM4**2, 1/IncPotFFC[1]**2])
                PotenciaFinal=sum(P_ICM4*IncP_ICM4)/sum(IncP_ICM4)
                IncePotenciaFinal=np.sqrt(1/sum(IncP_ICM4))

                c.drawString(200,197,str(np.format_float_scientific(PotenciaFinal, unique = False, precision=5))+" ± "+str(np.format_float_scientific(IncePotenciaFinal, unique = False, precision=5))+" W/A")

                
                 
                
            
            c.save()

            mesa='Excelente'
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Information)
            msg.setText("Informe creado con éxito")
            msg.setInformativeText(mesa)
            msg.setWindowTitle("PotCal 1.0")
            msg.exec_()


        except Exception as e:
            print(e)
            mesa='Error'
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Verificar datos ingresados")
            msg.setInformativeText(mesa)
            msg.setWindowTitle("PotCal 1.0")
            msg.exec_()
            
        # Método para agregar datos a la base de datos
    def cargarDatos(self, event):
        index = 0
        query = QSqlQuery() # Intancia del Query
        # Ejecutamos el query "select * from parametros"
        # El cual nos devolvera todos los datos de la tabla "parametros"
        query.exec_("select * from parametros") 

        # Iteramos los datos recividos
        while query.next():
            I1 = query.value(0) #---------------------------------------------- Corriente Cámara 1
            I2 = query.value(1) #---------------------------------------------- Corriente Cámara 2
            BW = query.value(2) #---------------------------------------------- bw
            Pot1 = query.value(3) #-------------------------------------------- Potencia Cámara 1
            Pot2 = query.value(4) #-------------------------------------------- Potencia Cámara 2
            Pot12 = query.value(5) #------------------------------------------- Potencia Cámara 1 y 2
            ICM4 = query.value(6) #--------------------------------------------- CM4
            Alfa1 = query.value(7) #------------------------------------------- Alfa 1
            Alfa2 = query.value(8) #------------------------------------------- Alfa 2
            Alfa12 = query.value(9) #------------------------------------------ Alfa 1 y 2
            Fq1 = query.value(10) #-------------------------------------------- Fq1
            Fq2 = query.value(11) #-------------------------------------------- Fq2
            ebeta1 = query.value(12) #----------------------------------------- ebeta1
            ebeta2 = query.value(13) #----------------------------------------- ebeta2

            # Ahora organizamos los datos en la tabla creada anteriormente
            self.table.setRowCount(index + 1)
            self.table.setItem(index, 0, QTableWidgetItem(I1))
            self.table.setItem(index, 1, QTableWidgetItem(I2))
            self.table.setItem(index, 2, QTableWidgetItem(BW))
            self.table.setItem(index, 3, QTableWidgetItem(Pot1))
            self.table.setItem(index, 4, QTableWidgetItem(Pot2))
            self.table.setItem(index, 5, QTableWidgetItem(Pot12))
            self.table.setItem(index, 6, QTableWidgetItem(ICM4))
            self.table.setItem(index, 7, QTableWidgetItem(Alfa1))
            self.table.setItem(index, 8, QTableWidgetItem(Alfa2))
            self.table.setItem(index, 9, QTableWidgetItem(Alfa12))
            self.table.setItem(index, 10, QTableWidgetItem(Fq1))
            self.table.setItem(index, 11, QTableWidgetItem(Fq2))
            self.table.setItem(index, 12, QTableWidgetItem(ebeta1))
            self.table.setItem(index, 13, QTableWidgetItem(ebeta2))
            
            

            index += 1

            # Método para insertar datos en la base de datos
    def insertarDatos(self, event):
        try:
            # Obtenemos los valores de los campos de texto
            I1=str(self.I1)+" ± "+str(self.IncerI1)
            I2=str(self.I2)+" ± "+str(self.IncerI2)
            Pot1=self.label_Pot1.text()
            Pot2=self.label_Pot2.text()
            Pot12=self.label_Pot12.text()
            Alfa1=self.label_alfa1.text()
            Alfa2=self.label_alfa2.text()
            Alfa12=self.label_alfa12.text()
            BW=str(int(round(self.BW,0)))
            ICM4=str(self.ICM4)+" ± "+str(self.ICM4inc)
            Fq1=str(self.Fq1)+" ± "+str(self.IncerFq1)            
            Fq2=str(self.Fq2)+" ± "+str(self.IncerFq2)            
            ebeta1=str(round(self.ebeta1,1))+" ± "+str(round(self.Errebeta1,1))
            ebeta2=str(round(self.ebeta2,1))+" ± "+str(round(self.Errebeta2,1)) 
            
            query = QSqlQuery() # Instancia de Query
            # Ejecutamos una sentencia para insertar los datos 
            # De los campos de texto
            query.exec_("insert into parametros values('{0}', '{1}', '{2}','{3}', '{4}','{5}', '{6}', '{7}','{8}', '{9}', '{10}','{11}', '{12}', '{13}')".format(I1, I2, BW, Pot1, Pot2, Pot12, ICM4, Alfa1, Alfa2, Alfa12, Fq1, Fq2, ebeta1, ebeta2))
            self.cargarDatos(event)
        except Exception as e:
            print(e)
        # Mpetodo para eliminar datos d ela base de datos
    def eliminarDatos(self, event):
        try:
            # select = fila seleccionada en la tabla
            selected = self.table.currentIndex()
            if not selected.isValid() or len(self.table.selectedItems()) < 1:
                return

            I1 = self.table.selectedItems()[0] # valor de tabla

            query = QSqlQuery() # instancia de Query
            # Ejecutamos una sentencia. Eliminara toda fila cuyo
            # Valor de id sea igual al seleccionado
            query.exec_("delete from parametros where I1 = " + I1.text())

            self.table.removeRow(selected.row()) # Removemos la fila
            self.table.setCurrentIndex(QModelIndex())
        except Exception as e:
            print(e)

        # Método para crear la base de datos
    def db_connect(self, filename, server): # Recibe dos parametros: nombre de la base de datos, y el tipo.
        db = QSqlDatabase.addDatabase(server) # Creamos la base de datos
        db.setDatabaseName(filename) # Le asignamos un nombre
        if not db.open(): # En caso de que no se abra
            QMessageBox.critical(None, "Error al abrir la base de datos.nn"
                    "Click para cancelar y salir.", QMessageBox.Cancel)
            return False
        return True

        # Método para crear la tabla parametros
    def db_create(self):
        query = QSqlQuery() # Instancia de Query
        #Ejecutamos la sentencia para crear la tabla personas con 3 columnas
        query.exec_("create table parametros(I1 int primary key, "
                    "I2 varchar(20), BW varchar(20), Pot1 varchar(20), Pot2 varchar(20), Pot12 varchar(20), ICM4 varchar(20), Alfa1 varchar(20), Alfa2 varchar(20), Alfa12 varchar(20), Fq1 varchar(20), Fq2 varchar(20), ebeta1 varchar(20), ebeta2 varchar(20))")
        

        # Método para ejecutar la base de datos
    def init(self, filename, server):
        import os # Importamos os
        if not os.path.exists(filename):
            self.db_connect(filename, server) # Llamamos a "db_connect"
            self.db_create() # Llamamis a "db_create"
        else:
            self.db_connect(filename, server)





if __name__ == '__main__':
    app = QApplication(sys.argv) # Creamos una instancia de "QApplication"
    ejm = Ui_Principal() # Instancia de nuestra clase "Ui_Principal"
    # Llamamos al metodo "iinit"
    ejm.init('parametros', 'QSQLITE') 
    ejm.show() # Ejecutamos la ventana
    sys.exit(app.exec_()) # Cerramos el proceso







