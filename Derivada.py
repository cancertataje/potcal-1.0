import numpy as np


def AltaExactitud(func,t,P):
    if (len(P)==2):
        t=np.array(t)
        P=np.array(P)
        x1=float(P[0])
        h=0.001
        Inicio=x1-2*h
        Final=x1+2*h
        x=np.linspace(Inicio, Final, num=5)    
        Dfx=(-func(t,x[4],P[1])+8*func(t,x[3],P[1])-8*func(t,x[1],P[1])+func(t,x[0],P[1]))/(12*h)

        y1=float(P[1])
        Inicioy=y1-2*h
        Finaly=y1+2*h
        y=np.linspace(Inicioy, Finaly, num=5)
        Dfy=(-func(t,P[0],y[4])+8*func(t,P[0],y[3])-8*func(t,P[0],y[1])+func(t,P[0],y[0]))/(12*h)
        De=[Dfx, Dfy]
        return De
        
    if (len(P)==3):
        t=np.array(t)
        P=np.array(P)
        x1=float(P[0])
        h=0.001
        Inicio=x1-2*h
        Final=x1+2*h
        x=np.linspace(Inicio, Final, num=5)    
        Dfx=(-func(t,x[4],P[1],P[2])+8*func(t,x[3],P[1],P[2])-8*func(t,x[1],P[1],P[2])+func(t,x[0],P[1],P[2]))/(12*h)

        y1=float(P[1])
        Inicioy=y1-2*h
        Finaly=y1+2*h
        y=np.linspace(Inicioy, Finaly, num=5)
        Dfy=(-func(t,P[0],y[4],P[2])+8*func(t,P[0],y[3],P[2])-8*func(t,P[0],y[1],P[2])+func(t,P[0],y[0],P[2]))/(12*h)


        z1=float(P[2])
        Inicioz=z1-2*h
        Finalz=z1+2*h
        z=np.linspace(Inicioz, Finalz, num=5)
        Dfz=(-func(t,P[0],P[1],z[4])+8*func(t,P[0],P[1],z[3])-8*func(t,P[0],P[1],z[1])+func(t,P[0],P[1],z[0]))/(12*h)

        De=[Dfx, Dfy, Dfz]
        return De
