import numpy as np
import Derivada

def Newton(func,t,Y,P,sigma):
    sigma=np.array(sigma)
    t=np.array(t)
    Y=np.array(Y)
    P=np.array(P)
    gamma = 0.0001          # tamaño del paso
    I=25000                 # número de iteraciones
    N=len(t)                # elementos de la muestra

    J=np.zeros([N,len(P)])  # matriz Jabobiana vacia
    W=np.eye(len(t))        # matriz de ceros (0) con diagonal de unos (1)
    YProm=sum(Y)/len(Y)
    
    St=0
    for i in range(0,len(Y)):
        St=St+(Y[i]-YProm)**2
    
    if(len(P)==2):
        A1=P[0]
        alfa1=P[1]
        Sr=0
        g=1/sigma
        w=g/sum(g)
        for i in range(0,I):
            P=[A1, alfa1]
            Der=Derivada.AltaExactitud(func,t,P)
            J[:,0]=Der[0]
            J[:,1]=Der[1]
            
            ri=Y-func(t,A1,alfa1) # residual tender a cero

            delta=np.dot(np.dot(np.linalg.inv(np.dot(np.dot(J.T,W), J)), J.T),np.dot( W,(ri*w).T)) # nuevo paso
            A1=A1+delta[0]        # actualiz1 A1 para próximo paso
            alfa1=alfa1+delta[1]  # actualiza alfa1 para próximo paso

        for i in range(0,len(ri)):
            Sr=Sr+(ri[i])**2      # suma de los cuadrados de los residuos
            

        Syx=Sr/(N-(len(P)))       # Error estándar al cuadrado

        R=(St-Sr)/St              # Coeficiente de determinación
        
        Dat=[A1,alfa1]
        Cov=np.linalg.inv(np.dot(np.dot(J.T,W), J))*Syx   # Matriz Covariante

        return Dat, Cov, R
            
        
        
    if(len(P)==3):
        
        A1=P[0]
        alfa1=P[1]
        Rd1=P[2]
        Sr=0
        g=1/sigma
        w=g/sum(g)
        for i in range(0,I):
            P=[A1, alfa1, Rd1]
            Der=Derivada.AltaExactitud(func,t,P)
            J[:,0]=Der[0]
            J[:,1]=Der[1]
            J[:,2]=Der[2]
                    
            ri=Y-func(t,A1,alfa1,Rd1) # residual tender a cero

            delta=np.dot(np.dot(np.linalg.inv(np.dot(np.dot(J.T,W), J)), J.T),np.dot( W,(ri*w).T)) # nuevo paso 
            A1=A1+delta[0]        # actualiz1 A1 para próximo paso
            alfa1=alfa1+delta[1]  # actualiza alfa1 para próximo paso
            Rd1=Rd1+delta[2]      # actualiza Rd1 para próximo paso

        for i in range(0,len(ri)):
            Sr=Sr+(ri[i])**2      # suma de los cuadrados de los residuos
        
        Syx=Sr/(N-(len(P)))       # Error estándar al cuadrado
        R=(St-Sr)/St              # Coeficiente de determinación
        
        Dat=[A1,alfa1,Rd1]
        Cov=np.linalg.inv(np.dot(np.dot(J.T,W), J))*Syx  # Matriz Covariante

        return Dat, Cov, R
        
    



        
