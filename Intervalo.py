import numpy as np

def separacion(a):
    a=np.array(a)
    c=np.zeros(len(a))
    for i in range(0,len(a)):
        A=max(np.array(np.where( (a >= a[i]*0.98) & (a <= a[i]*1.02))))
        c[i]=int(A[len(A)-1])
    b=np.sort(np.array(list(set(c))))

    return b
