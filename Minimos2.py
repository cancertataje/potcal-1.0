import numpy as np

def Cuadrados(X,Y,sigma,numexpo):
    try:
        sigma=np.array(sigma)
    except:
        sigma=np.ones(len(Y))
    X=np.array(X)
    Y=np.array(Y)
    numexpo=int(numexpo)
    YProm=sum(Y)/len(Y)
    w=1/sigma
    g=w
    St=0
    Sr=0
    for i in range(0,len(Y)):
        St=St+(Y[i]-YProm)**2
    
    if numexpo==1:
        Bcal=np.array([np.ones(len(X))*g, X*g]).T
    if numexpo==2:
        Bcal=np.array([np.ones(len(X))*g, X*g, (X**2)*g]).T
    if numexpo==3:
        Bcal=np.array([np.ones(len(X))*g, X*g, (X**2)*g, (X**3)*g]).T
    if numexpo==4:
        Bcal=np.array([np.ones(len(X))*g, X*g, (X**2)*g, (X**3)*g, (X**4)*g]).T
    if numexpo==5:
        Bcal=np.array([np.ones(len(X))*g, X*g, (X**2)*g, (X**3)*g, (X**4)*g, (X**5)*g]).T
    if numexpo==6:
        Bcal=np.array([np.ones(len(X))*g, X*g, (X**2)*g, (X**3)*g, (X**4)*g, (X**5)*g, (X**6)*g]).T
    if numexpo==7:
        Bcal=np.array([np.ones(len(X))*g, X*g, (X**2)*g, (X**3)*g, (X**4)*g, (X**5)*g, (X**6)*g, (X**7)*g]).T

    BtB=np.dot(np.transpose(Bcal),Bcal)
    BtQ=np.dot(np.transpose(Bcal),Y*g)
    a=np.dot(np.linalg.inv(BtB),BtQ)

    if numexpo==1:
        Sr=sum((Y-(a[0]+a[1]*X))**2)
        Syx=Sr/(len(Y)-2)
        R=(St-Sr)/St
        
    
    cov=np.linalg.inv(BtB)*Syx

    return a, cov, R
