import numpy as np

def GaussLorentz(X, sig, x):
    X=np.array(X)
    ### Distribución Gaussiana ###
    FG=np.exp(-(X-x)**2/(2*sig**2))/np.sqrt(2*np.pi*sig**2)
    NG=sum(FG)
    fg=FG*NG


    ### Distribución Lorenziana ###
    FWHM=2*np.sqrt(2*np.log(2))*sig
    FL=1/(np.pi*FWHM*(1+((X-x)/FWHM)**2))
    NL=sum(FL)
    fl=FL*NL

    ff=0.9*fg+0.1*fl

    return ff
